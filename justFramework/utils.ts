export class Utils  {
	/**
	 * @param {number} len
	 * @param {string} charSet
	 * @returns {string}
	 */
	static randomString(len: number, charSet?: string): string {
		charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		let randomString = '';
		let i;
		for (i = 0; i < len; i++) {
			let randomPoz = Math.floor(Math.random() * charSet.length);
			randomString += charSet.substring(randomPoz, randomPoz + 1);
		}
		return randomString;
	}

	/**
	 * @returns {string}
	 */
	static getBrowserLanguage(): string {
		// @ts-ignore
		let [lang, locale] = (((navigator.userLanguage || navigator.language).replace('-', '_')).toLowerCase()).split('_');
		return lang;
	}

	/**
	 * @param {string} selector
	 * @param callback
	 */
	static waitForElement(selector: string, callback) {
		if ($(selector).length) {
			callback();
		} else {
			setTimeout(function () {
				Utils.waitForElement(selector, callback);
			}, 5);
		}
	}

	/**
	 * @param {string} key
	 * @returns {string}
	 */
	static removeUrlParam(key: string) {
		if (!key) return;

		let url = document.location.href;
		let urlparts = url.split('?');

		if (urlparts.length >= 2) {
			let urlBase = urlparts.shift();
			let queryString = urlparts.join("?");

			let prefix = encodeURIComponent(key) + '=';
			let pars = queryString.split(/[&;]/g);
			for (let i = pars.length; i-- > 0;)
				if (pars[i].lastIndexOf(prefix, 0) !== -1)
					pars.splice(i, 1);
			url = urlBase + '?' + pars.join('&');
			window.history.pushState('', document.title, url);

		}
		return url;
	}

	/**
	 * @param {string} str
	 * @returns {string}
	 */
	static capitalize(str: string) {
		return str.toLowerCase().split(' ').map(function(word) {
			return word.replace(word[0], word[0].toUpperCase());
		}).join(' ');

	}

	/**
	 * @param {string} str
	 * @returns {string}
	 */
	static stringToUrl(str: string) {
		let charlist;
		charlist = [
			['Á','A'], ['Ä','A'], ['Č','C'], ['Ç','C'], ['Ď','D'], ['É','E'], ['Ě','E'],
			['Ë','E'], ['Í','I'], ['Ň','N'], ['Ó','O'], ['Ö','O'], ['Ř','R'], ['Š','S'],
			['Ť','T'], ['Ú','U'], ['Ů','U'], ['Ü','U'], ['Ý','Y'], ['Ž','Z'], ['á','a'],
			['ä','a'], ['č','c'], ['ç','c'], ['ď','d'], ['é','e'], ['ě','e'], ['ë','e'],
			['í','i'], ['ň','n'], ['ó','o'], ['ö','o'], ['ř','r'], ['š','s'], ['ť','t'],
			['ú','u'], ['ů','u'], ['ü','u'], ['ý','y'], ['ž','z']
		];
		for (let i in charlist) {
			let re = new RegExp(charlist[i][0],'g');
			str = str.replace(re, charlist[i][1]);
		}

		str = str.replace(/[^a-z0-9]/ig, '-');
		str = str.replace(/\-+/g, '-');
		if (str[0] == '-') {
			str = str.substring(1, str.length);
		}
		if (str[str.length - 1] == '-') {
			str = str.substring(0, str.length - 1);
		}

		return str.toLowerCase();
	}
}