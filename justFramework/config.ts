export class Config {
	private static values: object = {};

	/**
	 * @param {string} id
	 * @param {any} fallback
	 * @returns {any}
	 */
	static get(id: string, fallback = undefined): any {
		return Config.values[id] !== undefined ? Config.values[id] : fallback;
	}

	/**
	 * @param {string} id
	 * @param {any} value
	 */
	static set(id: string, value: any): void {
		Config.values[id] = value;
	}
}