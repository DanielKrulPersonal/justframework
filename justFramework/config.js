(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var Config = /** @class */ (function () {
        function Config() {
        }
        /**
         * @param {string} id
         * @param {any} fallback
         * @returns {any}
         */
        Config.get = function (id, fallback) {
            if (fallback === void 0) { fallback = undefined; }
            return Config.values[id] !== undefined ? Config.values[id] : fallback;
        };
        /**
         * @param {string} id
         * @param {any} value
         */
        Config.set = function (id, value) {
            Config.values[id] = value;
        };
        Config.values = {};
        return Config;
    }());
    exports.Config = Config;
});

//# sourceMappingURL=config.js.map
