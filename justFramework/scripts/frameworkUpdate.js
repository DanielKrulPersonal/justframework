const consts = require('./assets');
const os = require('os');
const Git = require('nodegit');
const fs = require('fs-extra');

console.clear();
console.log(consts.logo);

(function updateFramework() {
	let tempFolder = os.tmpdir() + '/justFramework/';

	fs.removeSync(tempFolder);
	Git.Clone(consts.repoUrl, tempFolder)
		.then(function () {
			fs.copySync(tempFolder + 'justFramework/', 'justFramework/');
			console.log('Update was complete');
		})
		.catch(function (err) {
			console.log(err);
		});
})();
