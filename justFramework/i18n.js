(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "justFramework/config", "justFramework/utils", "justFramework/routes"], factory);
    }
})(function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var config_1 = require("justFramework/config");
    var utils_1 = require("justFramework/utils");
    var routes_1 = require("justFramework/routes");
    var i18n = /** @class */ (function () {
        function i18n() {
        }
        /**
         * @param {string} id
         * @param {string} page
         * @returns {string}
         */
        i18n.get = function (id, page) {
            if (page === void 0) { page = null; }
            if (page === null)
                page = routes_1.Routes.actualPath;
            if (config_1.Config.get('pages').indexOf(page) === -1)
                throw new Error('There is no page: "' + page + '"!');
            if (i18n.setIds[id + '//' + page] !== undefined)
                return i18n.setIds[id + '//' + page];
            var translateFileFolder = '/pages/' + page + '/i18n/';
            var translateFileName = (config_1.Config.get('i18n') === 'auto' ? utils_1.Utils.getBrowserLanguage() : config_1.Config.get('i18n')) + '.json';
            var translatedString = i18n.getTranslateFile(translateFileFolder, translateFileName, id);
            if (!translatedString === undefined)
                throw new Error('Translation for "' + id + '" on page "' + page + '" not found!');
            return translatedString;
        };
        /**
         * @param {string} translateFileFolder
         * @param {string} translateFileName
         * @param {string} id
         * @returns {string}
         */
        i18n.getTranslateFile = function (translateFileFolder, translateFileName, id) {
            if (i18n.missingTranslationFiles.indexOf(translateFileFolder + translateFileName) > -1)
                translateFileName = 'default.json';
            var translatedString = undefined, fullFileName = translateFileFolder + translateFileName;
            if (i18n.cachedTranslationFiles[fullFileName])
                return i18n.cachedTranslationFiles[fullFileName][id];
            $.ajax({
                url: fullFileName,
                dataType: 'json',
                async: false,
                cache: config_1.Config.get('cache'),
                success: function (json) {
                    translatedString = json[id];
                    i18n.cachedTranslationFiles[fullFileName] = json;
                },
                statusCode: {
                    404: function () {
                        i18n.missingTranslationFiles.push(fullFileName);
                        translatedString = i18n.getTranslateFile(translateFileFolder, 'default.json', id);
                    }
                }
            });
            return translatedString;
        };
        /**
         * @param {string} id
         * @param {string} value
         * @param {string} page
         */
        i18n.set = function (id, value, page) {
            if (page === void 0) { page = null; }
            if (page === null)
                page = routes_1.Routes.actualPath;
            i18n.setIds[id + '//' + page] = value;
        };
        i18n.setIds = {};
        i18n.cachedTranslationFiles = {};
        i18n.missingTranslationFiles = [];
        return i18n;
    }());
    exports.i18n = i18n;
});

//# sourceMappingURL=i18n.js.map
