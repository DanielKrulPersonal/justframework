import {i18n} from "justFramework/i18n";
import {Config} from "justFramework/config";

export class Routes {
	readonly path: string;
	private subPage: string = null;
	private pages: string[] = [];
	static actualPath: string = null;

	public constructor() {
		let _this = this;
		this.path = window.location.pathname;
		this.pages = Config.get('pages', []);

		this.loadAllPages(function() {
			_this.runPageInPath();
		});
	}

	/**
	 * @param {function} done
	 */
	private loadAllPages(done: any): void {
		let pagesWithFullPath: string[] = [];
		this.pages.forEach(function(page) {
			pagesWithFullPath.push('/pages/'+ page +'/'+ page +'.model.js');
			pagesWithFullPath.push('/pages/'+ page +'/'+ page +'.controller.js');
		});

		if(!pagesWithFullPath.length) return;

		require(pagesWithFullPath, function () {
			done();
		});
	}

	private runPageInPath(): void {
		let _this = this;
		let urlParts = _this.path.split('/');

		this.pages.forEach(function (page) {
			if(_this.path === '/' || page === 'index') return;
			if(i18n.get('page-url', page) === urlParts[1]) Routes.actualPath = page;
		});
		if(Routes.actualPath === null) {
			this.pages.forEach(function (page) {
				if(urlParts[1] == page)
					Routes.actualPath = page;
			});
		}
		if(urlParts[2] !== undefined) this.subPage = urlParts[2];
		if(this.path == '/') {
			Routes.actualPath = 'index';
			this.runPage('index');
		} else if(Routes.actualPath !== null) {
			this.runPage(Routes.actualPath);
		} else {
			console.log('jsme někde v prdeli, možná bude tato url patřit něčemu nepříjemnému, ale spíše zobrazíme 404');
		}
	}

	/**
	 * @param {string} page
	 */
	private runPage(page: string): void {
		// run model
		let modelInstance, controllerInstance;

		try {
			modelInstance = new window[page + 'Model']();
			controllerInstance = new window[page + 'Controller']();
		} catch (e) {
			throw new Error('Model or controller of page "' + page + '" was not found or is not declared! Error: ' + e);
		}
		window[page + 'ModelInstance'] = modelInstance;
		window[page + 'ControllerInstance'] = controllerInstance;


		modelInstance.runEvent();
		controllerInstance.initialize();
		controllerInstance.setRoutes(this.subPage);
		controllerInstance.runEvent();
	}
}
new Routes();
