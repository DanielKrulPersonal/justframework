(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var Model = /** @class */ (function () {
        function Model() {
        }
        Model.register = function () {
            var className = this.toString().match(/^function\s*([^\s(]+)/)[1];
            window[className] = this;
        };
        return Model;
    }());
    exports.Model = Model;
});

//# sourceMappingURL=model.js.map
