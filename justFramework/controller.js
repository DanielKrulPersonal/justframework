(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "justFramework/routes", "justFramework/mokaParser/mokaParser"], factory);
    }
})(function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var routes_1 = require("justFramework/routes");
    var mokaParser_1 = require("justFramework/mokaParser/mokaParser");
    var Controller = /** @class */ (function () {
        function Controller() {
            this.showMainTemplate = true;
            this.templateLoadFadeIn = false;
        }
        Controller.register = function () {
            var className = this.toString().match(/^function\s*([^\s(]+)/)[1];
            window[className] = this;
        };
        Controller.prototype.getModelData = function () {
            return window[routes_1.Routes.actualPath + 'ModelInstance'].data;
        };
        /**
         * @param {string} methodName
         * @param {function} callback
         * @param {object} param
         */
        Controller.prototype.getModelMethod = function (methodName, callback, param) {
            var data = window[routes_1.Routes.actualPath + 'ModelInstance'][methodName](param);
            callback(data);
        };
        /**
         * @param {object} variables
         */
        Controller.prototype.loadMainTemplate = function (variables) {
            if (variables === void 0) { variables = {}; }
            // here we have to load a main template
            if (this.showMainTemplate)
                this.loadTemplate(variables, routes_1.Routes.actualPath, routes_1.Routes.actualPath);
        };
        /**
         * @param {object} variables
         * @param {string} templateName
         * @param {string} page
         */
        Controller.prototype.loadTemplate = function (variables, templateName, page) {
            if (variables === void 0) { variables = {}; }
            var templateLocation = '/pages/' + page + '/views/' + templateName + '.moka';
            var mokaInstance = new mokaParser_1.MokaParser(templateLocation, variables);
            if (this.templateLoadFadeIn)
                $('body').hide().html(mokaInstance.getOutput()).fadeIn(250);
            else
                $('body').html(mokaInstance.getOutput());
        };
        /**
         * @param {string} subPage
         * @returns {string|number}
         */
        Controller.prototype.getSubPageId = function (subPage) {
            return subPage.replace(/-.*/gm, '');
        };
        return Controller;
    }());
    exports.Controller = Controller;
});

//# sourceMappingURL=controller.js.map
