(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "justFramework/i18n", "justFramework/config"], factory);
    }
})(function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var i18n_1 = require("justFramework/i18n");
    var config_1 = require("justFramework/config");
    var Routes = /** @class */ (function () {
        function Routes() {
            this.subPage = null;
            this.pages = [];
            var _this = this;
            this.path = window.location.pathname;
            this.pages = config_1.Config.get('pages', []);
            this.loadAllPages(function () {
                _this.runPageInPath();
            });
        }
        /**
         * @param {function} done
         */
        Routes.prototype.loadAllPages = function (done) {
            var pagesWithFullPath = [];
            this.pages.forEach(function (page) {
                pagesWithFullPath.push('/pages/' + page + '/' + page + '.model.js');
                pagesWithFullPath.push('/pages/' + page + '/' + page + '.controller.js');
            });
            if (!pagesWithFullPath.length)
                return;
            require(pagesWithFullPath, function () {
                done();
            });
        };
        Routes.prototype.runPageInPath = function () {
            var _this = this;
            var urlParts = _this.path.split('/');
            this.pages.forEach(function (page) {
                if (_this.path === '/' || page === 'index')
                    return;
                if (i18n_1.i18n.get('page-url', page) === urlParts[1])
                    Routes.actualPath = page;
            });
            if (Routes.actualPath === null) {
                this.pages.forEach(function (page) {
                    if (urlParts[1] == page)
                        Routes.actualPath = page;
                });
            }
            if (urlParts[2] !== undefined)
                this.subPage = urlParts[2];
            if (this.path == '/') {
                Routes.actualPath = 'index';
                this.runPage('index');
            }
            else if (Routes.actualPath !== null) {
                this.runPage(Routes.actualPath);
            }
            else {
                console.log('jsme někde v prdeli, možná bude tato url patřit něčemu nepříjemnému, ale spíše zobrazíme 404');
            }
        };
        /**
         * @param {string} page
         */
        Routes.prototype.runPage = function (page) {
            // run model
            var modelInstance, controllerInstance;
            try {
                modelInstance = new window[page + 'Model']();
                controllerInstance = new window[page + 'Controller']();
            }
            catch (e) {
                throw new Error('Model or controller of page "' + page + '" was not found or is not declared! Error: ' + e);
            }
            window[page + 'ModelInstance'] = modelInstance;
            window[page + 'ControllerInstance'] = controllerInstance;
            modelInstance.runEvent();
            controllerInstance.initialize();
            controllerInstance.setRoutes(this.subPage);
            controllerInstance.runEvent();
        };
        Routes.actualPath = null;
        return Routes;
    }());
    exports.Routes = Routes;
    new Routes();
});

//# sourceMappingURL=routes.js.map
