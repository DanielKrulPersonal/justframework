export abstract class Model  {
	static register(): void {
		let className = this.toString().match(/^function\s*([^\s(]+)/)[1];
		window[className] = this;
	}
	protected data: any;
	abstract runEvent();
}