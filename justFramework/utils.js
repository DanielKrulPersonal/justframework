(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var Utils = /** @class */ (function () {
        function Utils() {
        }
        /**
         * @param {number} len
         * @param {string} charSet
         * @returns {string}
         */
        Utils.randomString = function (len, charSet) {
            charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var randomString = '';
            var i;
            for (i = 0; i < len; i++) {
                var randomPoz = Math.floor(Math.random() * charSet.length);
                randomString += charSet.substring(randomPoz, randomPoz + 1);
            }
            return randomString;
        };
        /**
         * @returns {string}
         */
        Utils.getBrowserLanguage = function () {
            // @ts-ignore
            var _a = (((navigator.userLanguage || navigator.language).replace('-', '_')).toLowerCase()).split('_'), lang = _a[0], locale = _a[1];
            return lang;
        };
        /**
         * @param {string} selector
         * @param callback
         */
        Utils.waitForElement = function (selector, callback) {
            if ($(selector).length) {
                callback();
            }
            else {
                setTimeout(function () {
                    Utils.waitForElement(selector, callback);
                }, 5);
            }
        };
        /**
         * @param {string} key
         * @returns {string}
         */
        Utils.removeUrlParam = function (key) {
            if (!key)
                return;
            var url = document.location.href;
            var urlparts = url.split('?');
            if (urlparts.length >= 2) {
                var urlBase = urlparts.shift();
                var queryString = urlparts.join("?");
                var prefix = encodeURIComponent(key) + '=';
                var pars = queryString.split(/[&;]/g);
                for (var i = pars.length; i-- > 0;)
                    if (pars[i].lastIndexOf(prefix, 0) !== -1)
                        pars.splice(i, 1);
                url = urlBase + '?' + pars.join('&');
                window.history.pushState('', document.title, url);
            }
            return url;
        };
        /**
         * @param {string} str
         * @returns {string}
         */
        Utils.capitalize = function (str) {
            return str.toLowerCase().split(' ').map(function (word) {
                return word.replace(word[0], word[0].toUpperCase());
            }).join(' ');
        };
        /**
         * @param {string} str
         * @returns {string}
         */
        Utils.stringToUrl = function (str) {
            var charlist;
            charlist = [
                ['Á', 'A'], ['Ä', 'A'], ['Č', 'C'], ['Ç', 'C'], ['Ď', 'D'], ['É', 'E'], ['Ě', 'E'],
                ['Ë', 'E'], ['Í', 'I'], ['Ň', 'N'], ['Ó', 'O'], ['Ö', 'O'], ['Ř', 'R'], ['Š', 'S'],
                ['Ť', 'T'], ['Ú', 'U'], ['Ů', 'U'], ['Ü', 'U'], ['Ý', 'Y'], ['Ž', 'Z'], ['á', 'a'],
                ['ä', 'a'], ['č', 'c'], ['ç', 'c'], ['ď', 'd'], ['é', 'e'], ['ě', 'e'], ['ë', 'e'],
                ['í', 'i'], ['ň', 'n'], ['ó', 'o'], ['ö', 'o'], ['ř', 'r'], ['š', 's'], ['ť', 't'],
                ['ú', 'u'], ['ů', 'u'], ['ü', 'u'], ['ý', 'y'], ['ž', 'z']
            ];
            for (var i in charlist) {
                var re = new RegExp(charlist[i][0], 'g');
                str = str.replace(re, charlist[i][1]);
            }
            str = str.replace(/[^a-z0-9]/ig, '-');
            str = str.replace(/\-+/g, '-');
            if (str[0] == '-') {
                str = str.substring(1, str.length);
            }
            if (str[str.length - 1] == '-') {
                str = str.substring(0, str.length - 1);
            }
            return str.toLowerCase();
        };
        return Utils;
    }());
    exports.Utils = Utils;
});

//# sourceMappingURL=utils.js.map
