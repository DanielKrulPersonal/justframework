export class Pagination {
	readonly param: string;
	readonly startPage: number;
	readonly activePage: number;
	readonly numOfItems: number;
	readonly perPage: number;
	readonly maximumNumberOfViewedPages: number = 6; //must be even

	public constructor(param: string, numOfItems: number, perPage: number, startPage: number = 1) {
		this.param = param;
		this.startPage = startPage;
		this.numOfItems = numOfItems;
		this.perPage = perPage;

		let urlParams = new URLSearchParams(window.location.search);
		this.activePage = urlParams.has(this.param) ? parseInt(urlParams.get(this.param)) : this.startPage;
	}

	public get():object {
		let possiblePages: number[] = [];
		let maxNumberOfPages: number = Math.ceil(this.numOfItems / this.perPage);
		// to left
		for (let i = 1; i <= this.maximumNumberOfViewedPages / 2; i++) {
			if(i > maxNumberOfPages) break;
			possiblePages.push(i);
		}
		// to right
		for (let i = this.maximumNumberOfViewedPages / 2; i < this.maximumNumberOfViewedPages; i++) {
			if(i + 1 >= maxNumberOfPages) break;
			possiblePages.push(i + 1);
		}

		return {
			'isPreviousBlocked': this.activePage - 1 <= 0,
			'previousPage': this.activePage - 1,
			'nextPage': this.activePage + 1,
			'isNextBlocked': this.activePage * this.perPage >= this.numOfItems,
			'activePage': this.activePage,
			'possiblePages': possiblePages,
		}
	}
}

