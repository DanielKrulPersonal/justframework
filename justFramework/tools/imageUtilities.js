(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var ImageUtilities = /** @class */ (function () {
        function ImageUtilities() {
        }
        ImageUtilities.init = function () {
            ImageUtilities.isLazyLoad = false;
        };
        ImageUtilities.createCanvas = function (width, height) {
            var canvasSelector = $('#imageutilities-canvas');
            if ((canvasSelector.length) == 0) {
                // @ts-ignore
                ImageUtilities.canvasElement = $('<canvas hidden id="imageutilities-canvas" width="' + width + '" height="' + height + '">')[0];
                $('body').append(ImageUtilities.canvasElement);
            }
            else {
                // @ts-ignore
                ImageUtilities.canvasElement = canvasSelector[0];
                ImageUtilities.canvasElement.width = width;
                ImageUtilities.canvasElement.height = height;
            }
            ImageUtilities.canvasContext = ImageUtilities.canvasElement.getContext('2d');
            ImageUtilities.canvasContext.clearRect(0, 0, ImageUtilities.canvasElement.width, ImageUtilities.canvasElement.height);
        };
        ImageUtilities.crop = function (imageLocation, position, size, callback) {
            ImageUtilities.init();
            var img = new Image();
            img.onload = function () {
                // @ts-ignore
                ImageUtilities.createCanvas(this.width, this.height);
                var inMemCanvas = document.createElement('canvas');
                var inMemCtx = inMemCanvas.getContext('2d');
                switch (position) {
                    case ImageUtilities.POSITION_CENTER:
                        // @ts-ignore
                        var startX = this.width / 2, startY = this.height / 2;
                        if (startX < size.width)
                            startX = 0;
                        if (startY < size.height)
                            startY = 0;
                        // @ts-ignore
                        ImageUtilities.canvasContext.drawImage(img, startX, startY, size.width, size.height, 0, 0, size.width, size.height);
                        inMemCanvas.width = ImageUtilities.canvasElement.width;
                        inMemCanvas.height = ImageUtilities.canvasElement.height;
                        inMemCtx.drawImage(ImageUtilities.canvasElement, 0, 0);
                        ImageUtilities.canvasElement.width = size.width;
                        ImageUtilities.canvasElement.height = size.height;
                        ImageUtilities.canvasContext.drawImage(inMemCanvas, 0, 0);
                        break;
                }
                callback(ImageUtilities.canvasElement.toDataURL());
            };
            img.src = imageLocation;
        };
        ImageUtilities.enableLazyLoad = function () {
            ImageUtilities.isLazyLoad = true;
            return ImageUtilities;
        };
        ImageUtilities.cropInDOM = function (position, size) {
            function lazyLoad() {
                listOfImages.each(function (index, element) {
                    var jqueryImg = $(element);
                    // @ts-ignore
                    if (jqueryImg.data('imageutilities-done') != true && jqueryImg.visible(true)) {
                        ImageUtilities.crop(jqueryImg.attr('data-src'), position, size, function (imageString) {
                            jqueryImg.attr('src', imageString);
                        });
                        jqueryImg.data('imageutilities-done', true);
                    }
                });
            }
            var listOfImages = $('img[data-src]');
            if (ImageUtilities.isLazyLoad) {
                lazyLoad();
                $(window).scroll(lazyLoad);
            }
            else {
                listOfImages.each(function (index, element) {
                    var jqueryImg = $(element);
                    // @ts-ignore
                    if (jqueryImg.data('imageutilities-done') != true) {
                        ImageUtilities.crop(jqueryImg.attr('data-src'), position, size, function (imageString) {
                            jqueryImg.attr('src', imageString);
                        });
                        jqueryImg.data('imageutilities-done', true);
                    }
                });
            }
        };
        ImageUtilities.POSITION_CENTER = 1;
        return ImageUtilities;
    }());
    exports.ImageUtilities = ImageUtilities;
});

//# sourceMappingURL=imageUtilities.js.map
