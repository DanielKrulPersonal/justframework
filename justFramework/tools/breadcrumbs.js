(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var Breadcrumbs = /** @class */ (function () {
        function Breadcrumbs() {
            this.pages = [];
        }
        Breadcrumbs.prototype.add = function (name, url) {
            var isActive = url === '';
            this.pages.push({
                'name': name,
                'url': url,
                'isActive': isActive
            });
            return this;
        };
        Breadcrumbs.prototype.get = function () {
            return {
                'pages': this.pages
            };
        };
        return Breadcrumbs;
    }());
    exports.Breadcrumbs = Breadcrumbs;
});

//# sourceMappingURL=breadcrumbs.js.map
