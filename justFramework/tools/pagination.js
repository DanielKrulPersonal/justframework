(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var Pagination = /** @class */ (function () {
        function Pagination(param, numOfItems, perPage, startPage) {
            if (startPage === void 0) { startPage = 1; }
            this.maximumNumberOfViewedPages = 6; //must be even
            this.param = param;
            this.startPage = startPage;
            this.numOfItems = numOfItems;
            this.perPage = perPage;
            var urlParams = new URLSearchParams(window.location.search);
            this.activePage = urlParams.has(this.param) ? parseInt(urlParams.get(this.param)) : this.startPage;
        }
        Pagination.prototype.get = function () {
            var possiblePages = [];
            var maxNumberOfPages = Math.ceil(this.numOfItems / this.perPage);
            // to left
            for (var i = 1; i <= this.maximumNumberOfViewedPages / 2; i++) {
                if (i > maxNumberOfPages)
                    break;
                possiblePages.push(i);
            }
            // to right
            for (var i = this.maximumNumberOfViewedPages / 2; i < this.maximumNumberOfViewedPages; i++) {
                if (i + 1 >= maxNumberOfPages)
                    break;
                possiblePages.push(i + 1);
            }
            return {
                'isPreviousBlocked': this.activePage - 1 <= 0,
                'previousPage': this.activePage - 1,
                'nextPage': this.activePage + 1,
                'isNextBlocked': this.activePage * this.perPage >= this.numOfItems,
                'activePage': this.activePage,
                'possiblePages': possiblePages
            };
        };
        return Pagination;
    }());
    exports.Pagination = Pagination;
});

//# sourceMappingURL=pagination.js.map
