export class Breadcrumbs {
	private pages: object[] = [];

	public add(name: string, url: string): Breadcrumbs {
		let isActive: boolean = url === '';

		this.pages.push({
			'name': name,
			'url': url,
			'isActive': isActive
		});
		return this;
	}
	public get(): object {
		return {
			'pages': this.pages
		}
	}
}