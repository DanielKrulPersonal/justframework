export class ImageUtilities {
	private static canvasElement: HTMLCanvasElement;
	private static canvasContext: CanvasRenderingContext2D;
	private static isLazyLoad: boolean;

	public static readonly POSITION_CENTER = 1;

	private static init(): void {
		ImageUtilities.isLazyLoad = false;
	}

	private static createCanvas(width: number, height: number) {
		let canvasSelector = $('#imageutilities-canvas');

		if((canvasSelector.length) == 0) {
			// @ts-ignore
			ImageUtilities.canvasElement = $('<canvas hidden id="imageutilities-canvas" width="'+ width +'" height="'+ height +'">')[0];
			$('body').append(ImageUtilities.canvasElement);
		} else {
			// @ts-ignore
			ImageUtilities.canvasElement = canvasSelector[0];
			ImageUtilities.canvasElement.width = width;
			ImageUtilities.canvasElement.height = height;
		}

		ImageUtilities.canvasContext = ImageUtilities.canvasElement.getContext('2d');
		ImageUtilities.canvasContext.clearRect(0, 0, ImageUtilities.canvasElement.width, ImageUtilities.canvasElement.height);
	}

	public static crop(imageLocation: string, position: number, size: any, callback): void {
		ImageUtilities.init();

		let img = new Image();
		img.onload = function() {
			// @ts-ignore
			ImageUtilities.createCanvas(this.width, this.height);
			let inMemCanvas = document.createElement('canvas');
			let inMemCtx = inMemCanvas.getContext('2d');

			switch(position) {
				case ImageUtilities.POSITION_CENTER:
					// @ts-ignore
					let startX = this.width / 2, startY = this.height / 2;
					if (startX < size.width) startX = 0;
					if(startY < size.height) startY = 0;

					// @ts-ignore
					ImageUtilities.canvasContext.drawImage(img, startX, startY, size.width, size.height, 0, 0, size.width, size.height);
					inMemCanvas.width = ImageUtilities.canvasElement.width;
					inMemCanvas.height = ImageUtilities.canvasElement.height;
					inMemCtx.drawImage(ImageUtilities.canvasElement, 0, 0);
					ImageUtilities.canvasElement.width = size.width;
					ImageUtilities.canvasElement.height = size.height;
					ImageUtilities.canvasContext.drawImage(inMemCanvas, 0, 0);
					break;
			}

			callback(ImageUtilities.canvasElement.toDataURL());
		};
		img.src = imageLocation;
	}

	public static enableLazyLoad() {
		ImageUtilities.isLazyLoad = true;
		return ImageUtilities;
	}

	public static cropInDOM(position: number, size: object) {
		function lazyLoad() {
			listOfImages.each((index, element) => {

				let jqueryImg = $(element);
				// @ts-ignore
				if(jqueryImg.data('imageutilities-done') != true && jqueryImg.visible(true)) {
					ImageUtilities.crop(jqueryImg.attr('data-src'), position, size, function (imageString) {
						jqueryImg.attr('src', imageString);
					});
					jqueryImg.data('imageutilities-done', true);
				}
			});
		}
		let listOfImages = $('img[data-src]');
		if(ImageUtilities.isLazyLoad) {
			lazyLoad();
			$(window).scroll(lazyLoad);
		} else {
			listOfImages.each((index, element) => {
				let jqueryImg = $(element);
				// @ts-ignore
				if(jqueryImg.data('imageutilities-done') != true) {
					ImageUtilities.crop(jqueryImg.attr('data-src'), position, size, function (imageString) {
						jqueryImg.attr('src', imageString);
					});
					jqueryImg.data('imageutilities-done', true);
				}
			});
		}
	}
}