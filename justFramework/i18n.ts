import {Config} from "justFramework/config";
import {Utils} from "justFramework/utils";
import {Routes} from "justFramework/routes";

export class i18n  {
	private static setIds: object = {};
	private static cachedTranslationFiles: object = {};
	private static missingTranslationFiles: string[] = [];

	/**
	 * @param {string} id
	 * @param {string} page
	 * @returns {string}
	 */
	static get(id: string, page: string = null): string {
		if(page === null) page = Routes.actualPath;
		if(Config.get('pages').indexOf(page) === -1)
			throw new Error('There is no page: "'+ page +'"!');
		if(i18n.setIds[id + '//' + page] !== undefined) return i18n.setIds[id + '//' + page];

		let translateFileFolder = '/pages/' + page + '/i18n/';
		let translateFileName = (Config.get('i18n') === 'auto' ? Utils.getBrowserLanguage() : Config.get('i18n')) + '.json';
		let translatedString = i18n.getTranslateFile(translateFileFolder, translateFileName, id);

		if(!translatedString === undefined) throw new Error('Translation for "'+ id +'" on page "'+ page +'" not found!');
		return translatedString;
	}

	/**
	 * @param {string} translateFileFolder
	 * @param {string} translateFileName
	 * @param {string} id
	 * @returns {string}
	 */
	private static getTranslateFile(translateFileFolder: string, translateFileName: string, id: string): string {
		if(i18n.missingTranslationFiles.indexOf(translateFileFolder + translateFileName) > -1)
			translateFileName = 'default.json';
		let translatedString = undefined,
			fullFileName = translateFileFolder + translateFileName;
		if(i18n.cachedTranslationFiles[fullFileName])
			return i18n.cachedTranslationFiles[fullFileName][id];

		$.ajax({
			url: fullFileName,
			dataType: 'json',
			async: false,
			cache: Config.get('cache'),
			success: function(json){
				translatedString = json[id];
				i18n.cachedTranslationFiles[fullFileName] = json;
			},
			statusCode: {
				404: function () {
					i18n.missingTranslationFiles.push(fullFileName);
					translatedString = i18n.getTranslateFile(translateFileFolder, 'default.json', id);
				}
			}
		});
		return translatedString;
	}

	/**
	 * @param {string} id
	 * @param {string} value
	 * @param {string} page
	 */
	static set(id, value, page = null): void {
		if(page === null) page = Routes.actualPath;
		i18n.setIds[id + '//' + page] = value;
	}
}