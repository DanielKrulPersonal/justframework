import {MokaParser} from "justFramework/mokaParser/mokaParser";
import {MokaParserRegexNames} from "justFramework/mokaParser/mokaParserRegexNames";

export class MokaParserSimpleVariable {
	private objectArray = [];
	private mokaParserInstance: MokaParser;

	public get(mokaParserInstance: MokaParser): void {
		this.mokaParserInstance = mokaParserInstance;
		this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(new RegExp('{\\s*' + MokaParserRegexNames.variableName.source + '\\s*}', 'gm'))).filter((obj) => obj);
		this.process();
	}

	private process(): void {
		let _this = this;
		let variable = {
			name: null,
			property: null,
			value: null
		};
		this.objectArray.forEach((block) => {
			variable.name = block.replace(/{\s*/gm, '').replace(/\s*}/gm, '');
			variable.property = MokaParserRegexNames.getPropertyFromVariableName(variable.name);
			if(variable.property)
				variable.name = variable.name.replace('.' + variable.property, '');

			variable.value = _this.mokaParserInstance.templateVariables[variable.name];
			if(variable.value !== undefined) {
				if(variable.property)
					_this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, variable.value[variable.property]);
				else
					_this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, variable.value);
			}
		});
	}
}