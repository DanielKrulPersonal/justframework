(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var MokaParserRegexNames = /** @class */ (function () {
        function MokaParserRegexNames() {
        }
        /**
        * @param {string} variableName
        * @returns {string}
        */
        MokaParserRegexNames.getPropertyFromVariableName = function (variableName) {
            var parts = variableName.split('.');
            if (parts.length === 1)
                return null;
            return parts[1].split('|')[0];
        };
        MokaParserRegexNames.variableName = /(?:(?![{}|])[a-žA-Ž0-9-_.])+/;
        MokaParserRegexNames.regexFunctionName = /[a-žA-Ž0-9-_]+/;
        MokaParserRegexNames.regexFunctionParam = /(?:(?![{}|])[a-žA-Ž0-9 &#\/'".:-])+/;
        return MokaParserRegexNames;
    }());
    exports.MokaParserRegexNames = MokaParserRegexNames;
});

//# sourceMappingURL=mokaParserRegexNames.js.map
