(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "justFramework/mokaParser/mokaParserRegexNames", "justFramework/utils"], factory);
    }
})(function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var mokaParserRegexNames_1 = require("justFramework/mokaParser/mokaParserRegexNames");
    var utils_1 = require("justFramework/utils");
    var MokaParserFunction = /** @class */ (function () {
        function MokaParserFunction() {
            this.objectArray = [];
        }
        MokaParserFunction.prototype.get = function (mokaParserInstance) {
            this.mokaParserInstance = mokaParserInstance;
            this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(new RegExp('{\\s*' + mokaParserRegexNames_1.MokaParserRegexNames.variableName.source + '\\s*\\|.*}', 'gm'))).filter(function (obj) { return obj; });
            this.process();
        };
        MokaParserFunction.prototype.process = function () {
            var _this = this;
            var variable = {
                name: null,
                property: null,
                value: null,
                "function": null,
                firstParam: null,
                secondParam: null
            };
            this.objectArray.forEach(function (block) {
                variable.name = block.replace(/{\s*/gm, '').replace(/\s*\|.*?}/gm, '');
                variable.property = mokaParserRegexNames_1.MokaParserRegexNames.getPropertyFromVariableName(variable.name);
                if (variable.property)
                    variable.name = variable.name.replace('.' + variable.property, '');
                variable.value = _this.mokaParserInstance.templateVariables[variable.name];
                if (variable.value !== undefined) {
                    if (variable.property)
                        variable.value = variable.value[variable.property];
                    var parts = block.split('|');
                    parts.shift();
                    parts.forEach(function (part) {
                        part = part.replace('}', '').trim();
                        if (new RegExp('^' + mokaParserRegexNames_1.MokaParserRegexNames.regexFunctionName.source + '$', 'gm').test(part)) {
                            variable["function"] = part;
                            variable.value = _this.processFunction(variable["function"], variable.value);
                        }
                        else if (new RegExp(mokaParserRegexNames_1.MokaParserRegexNames.regexFunctionName.source + '\\s*\\(\\s*[\'"]' + mokaParserRegexNames_1.MokaParserRegexNames.regexFunctionParam.source + '[\'"]\\s*\\)', 'gm').test(part)) {
                            variable["function"] = part.replace(new RegExp('\\s*\\(\\s*["\']' + mokaParserRegexNames_1.MokaParserRegexNames.regexFunctionParam.source + '["\']\\s*\\)', 'gm'), '');
                            variable.firstParam = part.replace(new RegExp(mokaParserRegexNames_1.MokaParserRegexNames.regexFunctionName.source + '\\s*\\(\\s*["\']', 'gm'), '').replace(/'\s*\)/gm, '');
                            variable.value = _this.processFunctionWithParam(variable["function"], variable.value, variable.firstParam);
                        }
                        else if (new RegExp(mokaParserRegexNames_1.MokaParserRegexNames.regexFunctionName.source + '\\s*\\(\\s*[\'"]' + mokaParserRegexNames_1.MokaParserRegexNames.regexFunctionParam.source + '[\'"]\\s*,\\s*[\'"]' + mokaParserRegexNames_1.MokaParserRegexNames.regexFunctionParam.source + '[\'"]\\s*\\)', 'gm').test(part)) {
                            variable["function"] = part.replace(/\s*\(.*?\)/gm, '');
                            variable.firstParam = part.replace(new RegExp(mokaParserRegexNames_1.MokaParserRegexNames.regexFunctionName.source + '\\s*\\(\\s*["\']', 'gm'), '').replace(/['"]\s*,\s*['"].*?\)/gm, '');
                            variable.secondParam = part.replace(/.*?['"]\s*,\s*['"]/gm, '').replace(/['"]\s*\)/gm, '');
                            variable.value = _this.processFunctionWithTwoParams(variable["function"], variable.value, variable.firstParam, variable.secondParam);
                        }
                    });
                    _this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, variable.value);
                }
            });
        };
        MokaParserFunction.prototype.processFunction = function (functionName, variableValue) {
            switch (functionName) {
                case 'uppercase':
                    variableValue = variableValue.toUpperCase();
                    break;
                case 'lowercase':
                    variableValue = variableValue.toLowerCase();
                    break;
                case 'length':
                    variableValue = variableValue.length;
                    break;
                case 'trim':
                    variableValue = $.trim(variableValue);
                    break;
                case 'striptags':
                    variableValue = $('<div/>').html(variableValue).text();
                    break;
                case 'firstUpper':
                    variableValue = variableValue[0].toUpperCase() + variableValue.slice(1);
                    break;
                case 'capitalize':
                    variableValue = utils_1.Utils.capitalize(variableValue);
                    break;
                case 'url':
                    variableValue = utils_1.Utils.stringToUrl(variableValue);
                    break;
                case 'breakline':
                    variableValue = variableValue.split("\n").join('<br />');
                    break;
                default:
                    throw new Error('Function "' + functionName + '" not found!');
            }
            return variableValue;
        };
        MokaParserFunction.prototype.processFunctionWithParam = function (functionName, variableValue, oneParam) {
            switch (functionName) {
                case 'repeat':
                    // @ts-ignore
                    variableValue = variableValue.repeat(oneParam);
                    break;
                case 'date':
                    // @ts-ignore
                    variableValue = new Date(variableValue * 1000).format(oneParam);
                    break;
                default:
                    throw new Error('Function "' + functionName + '" not found!');
            }
            return variableValue;
        };
        MokaParserFunction.prototype.processFunctionWithTwoParams = function (functionName, variableValue, oneParam, twoParam) {
            switch (functionName) {
                case 'replace':
                    variableValue = variableValue.replace(new RegExp(oneParam, 'gm'), twoParam);
                    break;
                case 'truncate':
                    variableValue = variableValue.substr(0, variableValue.lastIndexOf(' ', oneParam)) + twoParam;
                    break;
                default:
                    throw new Error('Function "' + functionName + '" not found!');
            }
            return variableValue;
        };
        return MokaParserFunction;
    }());
    exports.MokaParserFunction = MokaParserFunction;
});

//# sourceMappingURL=mokaParserFunction.js.map
