import {MokaParser} from "justFramework/mokaParser/mokaParser";
import {MokaParserRegexNames} from "justFramework/mokaParser/mokaParserRegexNames";
import {Routes} from "justFramework/routes";
import {MokaParserLoader} from "justFramework/mokaParser/mokaParserLoader";

export class MokaParserInclude {
	private objectArray = [];
	private mokaParserInstance: MokaParser;

	public get(mokaParserInstance: MokaParser): void {
		this.mokaParserInstance = mokaParserInstance;
		this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(new RegExp('{\\s*include\\s*[\'"]'+ MokaParserRegexNames.regexFunctionParam.source +'[\'"]\\s*}','gm'))).filter((obj) => obj);
		this.process();
	}

	private process(): void {
		let _this = this;
		let parsedBlock = {
			'templateName': null
		};
		this.objectArray.forEach((block) => {
			parsedBlock.templateName = block.replace(new RegExp('{.*?[\'"]', 'gm'), '').replace(new RegExp('[\'"]\\s*}', 'gm'), '');
			_this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, MokaParserLoader.getTemplateString('/pages/' + Routes.actualPath + '/views/' + parsedBlock.templateName + '.moka'));
		});
	}
}

export class MokaParserIncludePage {
	private objectArray = [];
	private mokaParserInstance: MokaParser;

	public get(mokaParserInstance: MokaParser): void {
		this.mokaParserInstance = mokaParserInstance;
		this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(new RegExp('{\\s*include\\s*[\'"]'+ MokaParserRegexNames.regexFunctionParam.source +'[\'"]\\s*,\\s*[\'"]'+ MokaParserRegexNames.regexFunctionParam.source +'[\'"]\\s*}','gm'))).filter((obj) => obj);
		this.process();
	}

	private process(): void {
		let _this = this;
		let parsedBlock = {
			'templateName': null,
			'page': null
		};
		this.objectArray.forEach((block) => {
			parsedBlock.templateName = block.replace(new RegExp('{.*?[\'"]', 'gm'), '').replace(new RegExp('[\'"]\\s*,\\s*[\'"].*', 'gm'), '');
			parsedBlock.page = block.replace(new RegExp('.*[\'"]\\s*,\\s*[\'"]', 'gm'), '').replace(new RegExp('[\'"]\\s*}', 'gm'), '');
			_this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, MokaParserLoader.getTemplateString('/pages/' + parsedBlock.page + '/views/' + parsedBlock.templateName + '.moka'));
		});
	}
}