import {Config} from "justFramework/config";

export class MokaParserLoader {
	private static templateCache: object = {};

	public static getTemplateString(templateLocation: string): string {
		let _this = this;
		if(this.templateCache[templateLocation])
			return this.templateCache[templateLocation];

		let output = '', error = false;
		$.ajax({
			url: templateLocation,
			dataType: 'text',
			async: false,
			cache: Config.get('cache'),
			success: function(raw){
				output = raw;
				_this.templateCache[templateLocation] = output;
			},
			statusCode: {
				404: function () {
					error = true;
				}
			}
		});
		if (error) throw new Error('Template "'+ location +'" not found!');
		return output;
	}
}