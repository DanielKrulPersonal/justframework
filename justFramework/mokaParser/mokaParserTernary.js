(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "justFramework/mokaParser/mokaParserRegexNames"], factory);
    }
})(function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var mokaParserRegexNames_1 = require("justFramework/mokaParser/mokaParserRegexNames");
    var MokaParserShortTernaryWithOperator = /** @class */ (function () {
        function MokaParserShortTernaryWithOperator() {
            this.objectArray = [];
        }
        MokaParserShortTernaryWithOperator.prototype.get = function (mokaParserInstance) {
            this.mokaParserInstance = mokaParserInstance;
            this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(new RegExp('{\\s*[\'"]' + mokaParserRegexNames_1.MokaParserRegexNames.regexFunctionParam.source + '[\'"]\\s*(?:==|!=|>=|<=|<|>)+\\s*[\'"]' + mokaParserRegexNames_1.MokaParserRegexNames.regexFunctionParam.source + '[\'"]\\s*\\?\\s*[\'"][\\s\\S]*?[\'"]\\s*}', 'gm'))).filter(function (obj) { return obj; });
            this.process();
        };
        MokaParserShortTernaryWithOperator.prototype.process = function () {
            var _this = this;
            var parsedBlock = {
                'leftPart': null,
                'operator': null,
                'rightPart': null
            };
            this.objectArray.forEach(function (block) {
                parsedBlock.leftPart = block.replace(new RegExp('{\\s*[\'"]', 'gm'), '').replace(new RegExp('[\'"]\\s*(?:==|!=|>=|<=|<|>)+\\s*[\'"]' + mokaParserRegexNames_1.MokaParserRegexNames.regexFunctionParam.source + '[\'"]\\s*\\?\\s*[\'"][\\s\\S]*[\'"]\\s*}', 'gm'), '');
                parsedBlock.operator = block.replace(new RegExp('{\\s*[\'"]' + mokaParserRegexNames_1.MokaParserRegexNames.regexFunctionParam.source + '[\'"]\\s*', 'gm'), '').replace(new RegExp('\\s*[\'"][\\s\\S]*}', 'gm'), '');
                parsedBlock.rightPart = block.replace(new RegExp('{\\s*[\'"]' + mokaParserRegexNames_1.MokaParserRegexNames.regexFunctionParam.source + '[\'"]\\s*(?:==|!=|>=|<=|<|>)+\\s*[\'"]', 'gm'), '').replace(new RegExp('[\'"]\\s*\\?\\s*[\'"][\\s\\S]*[\'"]\\s*}', 'gm'), '');
                var ternaryValue = block.replace(/{.*?\?\s*['"]+/gm, '').replace(/['"]+\s*}/gm, '');
                _this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, MokaParserShortTernaryWithOperator.compareValues(parsedBlock.leftPart, parsedBlock.rightPart, parsedBlock.operator, ternaryValue));
            });
        };
        MokaParserShortTernaryWithOperator.compareValues = function (leftPart, rightPart, operator, positiveValue, negativeValue) {
            if (positiveValue === void 0) { positiveValue = ''; }
            if (negativeValue === void 0) { negativeValue = ''; }
            switch (operator) {
                case '>':
                    return leftPart > rightPart ? positiveValue : negativeValue;
                case '<':
                    return leftPart < rightPart ? positiveValue : negativeValue;
                case '>=':
                    return leftPart >= rightPart ? positiveValue : negativeValue;
                case '<=':
                    return leftPart <= rightPart ? positiveValue : negativeValue;
                case '==':
                    return leftPart == rightPart ? positiveValue : negativeValue;
                case '!=':
                    return leftPart != rightPart ? positiveValue : negativeValue;
                default:
                    throw new Error('Invalid operator "' + operator + '"');
            }
        };
        return MokaParserShortTernaryWithOperator;
    }());
    exports.MokaParserShortTernaryWithOperator = MokaParserShortTernaryWithOperator;
    var MokaParserShortTernary = /** @class */ (function () {
        function MokaParserShortTernary() {
            this.objectArray = [];
        }
        MokaParserShortTernary.prototype.get = function (mokaParserInstance) {
            this.mokaParserInstance = mokaParserInstance;
            this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(new RegExp('{\\s*' + mokaParserRegexNames_1.MokaParserRegexNames.variableName.source + '\\s*\\?\\s*[\'"][\\s\\S]*?[\'"]\\s*}', 'gm'))).filter(function (obj) { return obj; });
            this.process();
        };
        MokaParserShortTernary.prototype.process = function () {
            var _this = this;
            var parsedBlock = {
                'variable': null,
                'property': null,
                'value': null
            };
            this.objectArray.forEach(function (block) {
                parsedBlock.variable = block.replace(new RegExp('{\\s*', 'gm'), '').replace(new RegExp('\\s*\\?.*?}', 'gm'), '');
                parsedBlock.property = mokaParserRegexNames_1.MokaParserRegexNames.getPropertyFromVariableName(parsedBlock.variable);
                if (parsedBlock.property) {
                    parsedBlock.variable = parsedBlock.variable.replace('.' + parsedBlock.property, '');
                }
                var ternaryValue = block.replace(/{.*?\?\s*['"]+/gm, '').replace(/['"]+\s*}/gm, '');
                if (_this.mokaParserInstance.templateVariables[parsedBlock.variable] !== undefined) {
                    parsedBlock.value = _this.mokaParserInstance.templateVariables[parsedBlock.variable];
                    if (parsedBlock.property)
                        parsedBlock.value = parsedBlock.value[parsedBlock.property];
                    if (parsedBlock.value) {
                        _this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, ternaryValue);
                    }
                    else {
                        _this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, '');
                    }
                }
            });
        };
        return MokaParserShortTernary;
    }());
    exports.MokaParserShortTernary = MokaParserShortTernary;
});

//# sourceMappingURL=mokaParserTernary.js.map
