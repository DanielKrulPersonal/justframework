(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "justFramework/config"], factory);
    }
})(function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var config_1 = require("justFramework/config");
    var MokaParserLoader = /** @class */ (function () {
        function MokaParserLoader() {
        }
        MokaParserLoader.getTemplateString = function (templateLocation) {
            var _this = this;
            if (this.templateCache[templateLocation])
                return this.templateCache[templateLocation];
            var output = '', error = false;
            $.ajax({
                url: templateLocation,
                dataType: 'text',
                async: false,
                cache: config_1.Config.get('cache'),
                success: function (raw) {
                    output = raw;
                    _this.templateCache[templateLocation] = output;
                },
                statusCode: {
                    404: function () {
                        error = true;
                    }
                }
            });
            if (error)
                throw new Error('Template "' + location + '" not found!');
            return output;
        };
        MokaParserLoader.templateCache = {};
        return MokaParserLoader;
    }());
    exports.MokaParserLoader = MokaParserLoader;
});

//# sourceMappingURL=mokaParserLoader.js.map
