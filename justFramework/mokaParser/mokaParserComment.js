(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var MokaParserComment = /** @class */ (function () {
        function MokaParserComment() {
            this.objectArray = [];
        }
        MokaParserComment.prototype.get = function (mokaParserInstance) {
            this.mokaParserInstance = mokaParserInstance;
            this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(/{\*[\s\S]*?\*}/gm)).filter(function (obj) { return obj; });
            this.process();
        };
        MokaParserComment.prototype.process = function () {
            var _this = this;
            this.objectArray.forEach(function (block) {
                _this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, '');
            });
        };
        return MokaParserComment;
    }());
    exports.MokaParserComment = MokaParserComment;
});

//# sourceMappingURL=mokaParserComment.js.map
