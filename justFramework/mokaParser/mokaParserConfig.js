(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "justFramework/mokaParser/mokaParserRegexNames", "justFramework/config"], factory);
    }
})(function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var mokaParserRegexNames_1 = require("justFramework/mokaParser/mokaParserRegexNames");
    var config_1 = require("justFramework/config");
    var MokaParserConfig = /** @class */ (function () {
        function MokaParserConfig() {
            this.objectArray = [];
        }
        MokaParserConfig.prototype.get = function (mokaParserInstance) {
            this.mokaParserInstance = mokaParserInstance;
            this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(new RegExp('{\\s*config\\s*[\'"]' + mokaParserRegexNames_1.MokaParserRegexNames.regexFunctionParam.source + '[\'"]\\s*}', 'gm'))).filter(function (obj) { return obj; });
            this.process();
        };
        MokaParserConfig.prototype.process = function () {
            var _this = this;
            var parsedBlock = {
                'value': null
            };
            this.objectArray.forEach(function (block) {
                parsedBlock.value = block.replace(new RegExp('{.*?[\'"]', 'gm'), '').replace(new RegExp('[\'"]\\s*}', 'gm'), '');
                _this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, config_1.Config.get(parsedBlock.value));
            });
        };
        return MokaParserConfig;
    }());
    exports.MokaParserConfig = MokaParserConfig;
});

//# sourceMappingURL=mokaParserConfig.js.map
