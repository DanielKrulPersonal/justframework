import {MokaParser} from "justFramework/mokaParser/mokaParser";
import {MokaParserRegexNames} from "justFramework/mokaParser/mokaParserRegexNames";

export class MokaParserShortTernaryWithOperator {
	private objectArray = [];
	private mokaParserInstance: MokaParser;

	public get(mokaParserInstance: MokaParser): void {
		this.mokaParserInstance = mokaParserInstance;
		this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(new RegExp('{\\s*[\'"]'+ MokaParserRegexNames.regexFunctionParam.source +'[\'"]\\s*(?:==|!=|>=|<=|<|>)+\\s*[\'"]'+ MokaParserRegexNames.regexFunctionParam.source +'[\'"]\\s*\\?\\s*[\'"][\\s\\S]*?[\'"]\\s*}', 'gm'))).filter((obj) => obj);
		this.process();
	}

	private process(): void {
		let _this = this;
		let parsedBlock = {
			'leftPart': null,
			'operator': null,
			'rightPart': null
		};

		this.objectArray.forEach((block) => {
			parsedBlock.leftPart = block.replace(new RegExp('{\\s*[\'"]','gm'), '').replace(new RegExp('[\'"]\\s*(?:==|!=|>=|<=|<|>)+\\s*[\'"]'+ MokaParserRegexNames.regexFunctionParam.source +'[\'"]\\s*\\?\\s*[\'"][\\s\\S]*[\'"]\\s*}', 'gm'), '');
			parsedBlock.operator = block.replace(new RegExp('{\\s*[\'"]'+ MokaParserRegexNames.regexFunctionParam.source +'[\'"]\\s*','gm'), '').replace(new RegExp('\\s*[\'"][\\s\\S]*}', 'gm'), '');
			parsedBlock.rightPart = block.replace(new RegExp('{\\s*[\'"]'+ MokaParserRegexNames.regexFunctionParam.source +'[\'"]\\s*(?:==|!=|>=|<=|<|>)+\\s*[\'"]','gm'), '').replace(new RegExp('[\'"]\\s*\\?\\s*[\'"][\\s\\S]*[\'"]\\s*}', 'gm'), '');

			let ternaryValue = block.replace(/{.*?\?\s*['"]+/gm, '').replace(/['"]+\s*}/gm, '');
			_this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, MokaParserShortTernaryWithOperator.compareValues(parsedBlock.leftPart, parsedBlock.rightPart, parsedBlock.operator, ternaryValue));
		});
	}

	static compareValues(leftPart: any, rightPart: any, operator: string, positiveValue = '', negativeValue = '') {
		switch(operator) {
			case '>':
				return leftPart > rightPart ? positiveValue : negativeValue;
			case '<':
				return leftPart < rightPart ? positiveValue : negativeValue;
			case '>=':
				return leftPart >= rightPart ? positiveValue : negativeValue;
			case '<=':
				return leftPart <= rightPart ? positiveValue : negativeValue;
			case '==':
				return leftPart == rightPart ? positiveValue : negativeValue;
			case '!=':
				return leftPart != rightPart ? positiveValue : negativeValue;
			default:
				throw new Error('Invalid operator "'+ operator +'"');
		}
	}
}

export class MokaParserShortTernary {
	private objectArray = [];
	private mokaParserInstance: MokaParser;

	public get(mokaParserInstance: MokaParser): void {
		this.mokaParserInstance = mokaParserInstance;
		this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(new RegExp('{\\s*'+ MokaParserRegexNames.variableName.source +'\\s*\\?\\s*[\'"][\\s\\S]*?[\'"]\\s*}', 'gm'))).filter((obj) => obj);
		this.process();
	}

	private process(): void {
		let _this = this;
		let parsedBlock = {
			'variable': null,
			'property': null,
			'value': null
		};

		this.objectArray.forEach((block) => {
			parsedBlock.variable = block.replace(new RegExp('{\\s*','gm'), '').replace(new RegExp('\\s*\\?.*?}', 'gm'), '');
			parsedBlock.property = MokaParserRegexNames.getPropertyFromVariableName(parsedBlock.variable);

			if(parsedBlock.property) {
				parsedBlock.variable = parsedBlock.variable.replace('.' + parsedBlock.property, '');
			}
			let ternaryValue = block.replace(/{.*?\?\s*['"]+/gm, '').replace(/['"]+\s*}/gm, '');
			if(_this.mokaParserInstance.templateVariables[parsedBlock.variable] !== undefined) {
				parsedBlock.value = _this.mokaParserInstance.templateVariables[parsedBlock.variable];
				if(parsedBlock.property)
					parsedBlock.value = parsedBlock.value[parsedBlock.property];

				if(parsedBlock.value) {
					_this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, ternaryValue);
				} else {
					_this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, '');
				}
			}
		});
	}
}