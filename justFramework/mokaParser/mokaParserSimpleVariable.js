(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "justFramework/mokaParser/mokaParserRegexNames"], factory);
    }
})(function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var mokaParserRegexNames_1 = require("justFramework/mokaParser/mokaParserRegexNames");
    var MokaParserSimpleVariable = /** @class */ (function () {
        function MokaParserSimpleVariable() {
            this.objectArray = [];
        }
        MokaParserSimpleVariable.prototype.get = function (mokaParserInstance) {
            this.mokaParserInstance = mokaParserInstance;
            this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(new RegExp('{\\s*' + mokaParserRegexNames_1.MokaParserRegexNames.variableName.source + '\\s*}', 'gm'))).filter(function (obj) { return obj; });
            this.process();
        };
        MokaParserSimpleVariable.prototype.process = function () {
            var _this = this;
            var variable = {
                name: null,
                property: null,
                value: null
            };
            this.objectArray.forEach(function (block) {
                variable.name = block.replace(/{\s*/gm, '').replace(/\s*}/gm, '');
                variable.property = mokaParserRegexNames_1.MokaParserRegexNames.getPropertyFromVariableName(variable.name);
                if (variable.property)
                    variable.name = variable.name.replace('.' + variable.property, '');
                variable.value = _this.mokaParserInstance.templateVariables[variable.name];
                if (variable.value !== undefined) {
                    if (variable.property)
                        _this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, variable.value[variable.property]);
                    else
                        _this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, variable.value);
                }
            });
        };
        return MokaParserSimpleVariable;
    }());
    exports.MokaParserSimpleVariable = MokaParserSimpleVariable;
});

//# sourceMappingURL=mokaParserSimpleVariable.js.map
