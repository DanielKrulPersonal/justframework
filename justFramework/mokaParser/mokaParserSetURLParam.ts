import {MokaParser} from "justFramework/mokaParser/mokaParser";
import {MokaParserRegexNames} from "justFramework/mokaParser/mokaParserRegexNames";

export class MokaParserSetURLParam {
	private objectArray = [];
	private mokaParserInstance: MokaParser;

	public get(mokaParserInstance: MokaParser): void {
		this.mokaParserInstance = mokaParserInstance;
		this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(new RegExp('{\\s*setURLParam\\s*[\'"]'+ MokaParserRegexNames.regexFunctionParam.source +'[\'"]\\s*,\\s*[\'"]'+ MokaParserRegexNames.regexFunctionParam.source +'[\'"]\\s*}','gm'))).filter((obj) => obj);
		this.process();
	}

	private process(): void {
		let _this = this;
		let parsedBlock = {
			'name': null,
			'value': null
		};
		this.objectArray.forEach((block) => {
			parsedBlock.name = block.replace(new RegExp('{.*?[\'"]', 'gm'), '').replace(new RegExp('[\'"]\\s*,\\s*[\'"].*', 'gm'), '');
			parsedBlock.value = block.replace(new RegExp('.*[\'"]\\s*,\\s*[\'"]', 'gm'), '').replace(new RegExp('[\'"]\\s*}', 'gm'), '');

			let urlParams = new URLSearchParams(window.location.search);
			urlParams.set(parsedBlock.name, parsedBlock.value);

			_this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, urlParams.toString());
		});
	}
}