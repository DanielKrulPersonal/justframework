(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "justFramework/mokaParser/mokaParserRegexNames", "justFramework/i18n"], factory);
    }
})(function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var mokaParserRegexNames_1 = require("justFramework/mokaParser/mokaParserRegexNames");
    var i18n_1 = require("justFramework/i18n");
    var MokaParserI18n = /** @class */ (function () {
        function MokaParserI18n() {
            this.objectArray = [];
        }
        MokaParserI18n.prototype.get = function (mokaParserInstance) {
            this.mokaParserInstance = mokaParserInstance;
            this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(new RegExp('{\\s*i18n\\s*[\'"]' + mokaParserRegexNames_1.MokaParserRegexNames.regexFunctionParam.source + '[\'"]\\s*}', 'gm'))).filter(function (obj) { return obj; });
            this.process();
        };
        MokaParserI18n.prototype.process = function () {
            var _this = this;
            var parsedBlock = {
                'value': null
            };
            this.objectArray.forEach(function (block) {
                parsedBlock.value = block.replace(new RegExp('{.*?[\'"]', 'gm'), '').replace(new RegExp('[\'"]\\s*}', 'gm'), '');
                _this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, i18n_1.i18n.get(parsedBlock.value));
            });
        };
        return MokaParserI18n;
    }());
    exports.MokaParserI18n = MokaParserI18n;
    var MokaParserI18nPage = /** @class */ (function () {
        function MokaParserI18nPage() {
            this.objectArray = [];
        }
        MokaParserI18nPage.prototype.get = function (mokaParserInstance) {
            this.mokaParserInstance = mokaParserInstance;
            this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(new RegExp('{\\s*i18n\\s*[\'"]' + mokaParserRegexNames_1.MokaParserRegexNames.regexFunctionParam.source + '[\'"]\\s*,\\s*[\'"]' + mokaParserRegexNames_1.MokaParserRegexNames.regexFunctionParam.source + '[\'"]\\s*}', 'gm'))).filter(function (obj) { return obj; });
            this.process();
        };
        MokaParserI18nPage.prototype.process = function () {
            var _this = this;
            var parsedBlock = {
                'value': null,
                'page': null
            };
            this.objectArray.forEach(function (block) {
                parsedBlock.value = block.replace(new RegExp('{.*?[\'"]', 'gm'), '').replace(new RegExp('[\'"]\\s*,\\s*[\'"].*', 'gm'), '');
                parsedBlock.page = block.replace(new RegExp('.*[\'"]\\s*,\\s*[\'"]', 'gm'), '').replace(new RegExp('[\'"]\\s*}', 'gm'), '');
                _this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, i18n_1.i18n.get(parsedBlock.value, parsedBlock.page));
            });
        };
        return MokaParserI18nPage;
    }());
    exports.MokaParserI18nPage = MokaParserI18nPage;
});

//# sourceMappingURL=mokaParserI18n.js.map
