import {MokaParser} from "justFramework/mokaParser/mokaParser";
import {MokaParserRegexNames} from "justFramework/mokaParser/mokaParserRegexNames";

export class MokaParserMetaAuthor {
	private objectArray = [];
	private mokaParserInstance: MokaParser;

	public get(mokaParserInstance: MokaParser): void {
		this.mokaParserInstance = mokaParserInstance;
		this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(new RegExp('{\\s*meta-author\\s*[\'"]'+ MokaParserRegexNames.regexFunctionParam.source +'[\'"]\\s*}','gm'))).filter((obj) => obj);
		this.process();
	}

	private process(): void {
		let _this = this;
		let parsedBlock = {
			'value': null
		};
		this.objectArray.forEach((block) => {
			parsedBlock.value = block.replace(new RegExp('{.*?[\'"]', 'gm'), '').replace(new RegExp('[\'"]\\s*}', 'gm'), '');
			$('meta[name=author]').attr('content', parsedBlock.value);
			_this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, '');
		});
	}
}

export class MokaParserMetaDescription {
	private objectArray = [];
	private mokaParserInstance: MokaParser;

	public get(mokaParserInstance: MokaParser): void {
		this.mokaParserInstance = mokaParserInstance;
		this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(new RegExp('{\\s*meta-description\\s*[\'"]'+ MokaParserRegexNames.regexFunctionParam.source +'[\'"]\\s*}','gm'))).filter((obj) => obj);
		this.process();
	}

	private process(): void {
		let _this = this;
		let parsedBlock = {
			'value': null
		};
		this.objectArray.forEach((block) => {
			parsedBlock.value = block.replace(new RegExp('{.*?[\'"]', 'gm'), '').replace(new RegExp('[\'"]\\s*}', 'gm'), '');
			$('meta[name=description]').attr('content', parsedBlock.value);
			_this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, '');
		});
	}
}

export class MokaParserSubtitle {
	private objectArray = [];
	private mokaParserInstance: MokaParser;

	public get(mokaParserInstance: MokaParser): void {
		this.mokaParserInstance = mokaParserInstance;
		this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(new RegExp('{\\s*subtitle\\s*[\'"]'+ MokaParserRegexNames.regexFunctionParam.source +'[\'"]\\s*}','gm'))).filter((obj) => obj);
		this.process();
	}

	private process(): void {
		let _this = this;
		let parsedBlock = {
			'value': null
		};
		this.objectArray.forEach((block) => {
			parsedBlock.value = block.replace(new RegExp('{.*?[\'"]', 'gm'), '').replace(new RegExp('[\'"]\\s*}', 'gm'), '');
			if(parsedBlock.value.trim() !== '') $(document).attr('title', parsedBlock.value + ' | ' + $(document).attr('title'));
			_this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, '');
		});
	}
}