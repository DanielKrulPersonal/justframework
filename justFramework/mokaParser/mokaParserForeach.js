(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "justFramework/mokaParser/mokaParserRegexNames"], factory);
    }
})(function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var mokaParserRegexNames_1 = require("justFramework/mokaParser/mokaParserRegexNames");
    var MokaParserForeach = /** @class */ (function () {
        function MokaParserForeach() {
            this.objectArray = [];
        }
        MokaParserForeach.prototype.get = function (mokaParserInstance) {
            this.mokaParserInstance = mokaParserInstance;
            this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(new RegExp('{\\s*foreach\\s*' + mokaParserRegexNames_1.MokaParserRegexNames.variableName.source + '\\s*as\\s*' + mokaParserRegexNames_1.MokaParserRegexNames.variableName.source + '\\s*}[\\s\\S]*?{\\s*\\/\\s*foreach\\s*}', 'gm'))).filter(function (obj) { return obj; });
            this.process();
        };
        MokaParserForeach.prototype.process = function () {
            var _this = this;
            var parsedBlock = {
                'variable': null,
                'property': null,
                'element': null,
                'content': null,
                'value': null
            };
            this.objectArray.forEach(function (block) {
                parsedBlock.variable = block.replace(/{\s*foreach\s*/gm, '').replace(new RegExp('\\s*as\\s*' + mokaParserRegexNames_1.MokaParserRegexNames.variableName.source + '\\s*}[\\s\\S]*?{\\s*\\/\\s*foreach\\s*}', 'gm'), '');
                parsedBlock.property = mokaParserRegexNames_1.MokaParserRegexNames.getPropertyFromVariableName(parsedBlock.variable);
                if (parsedBlock.property)
                    parsedBlock.variable = parsedBlock.variable.replace('.' + parsedBlock.property, '');
                parsedBlock.element = block.replace(new RegExp('{\\s*foreach\\s*' + mokaParserRegexNames_1.MokaParserRegexNames.variableName.source + '\\s*as\\s*', 'gm'), '').replace(/}[\s\S]*?{\s*\/\s*foreach\s*}/gm, '');
                parsedBlock.content = block.replace(new RegExp('{\\s*foreach\\s*' + mokaParserRegexNames_1.MokaParserRegexNames.variableName.source + '\\s*as\\s*' + mokaParserRegexNames_1.MokaParserRegexNames.variableName.source + '\\s*}\\n*\\t*', 'gm'), '').replace(/\n*\t*{\s*\/\s*foreach\s*}/gm, '');
                var result = '';
                if (_this.mokaParserInstance.templateVariables[parsedBlock.variable] !== undefined) {
                    // variable value is array or object
                    parsedBlock.value = _this.mokaParserInstance.templateVariables[parsedBlock.variable];
                    if (parsedBlock.property)
                        parsedBlock.value = parsedBlock.value[parsedBlock.property];
                    if (typeof parsedBlock.value !== 'object')
                        throw new Error('Variable "' + parsedBlock.variable + '" is not array!');
                    parsedBlock.value.forEach(function (val, index) {
                        var elementWithPrefix = parsedBlock.element + '-foreach-' + index;
                        result += parsedBlock.content.replace(new RegExp('{\\s*' + parsedBlock.element, 'gm'), '{' + elementWithPrefix);
                        var subindex = null;
                        var subindexElementArray = [].concat(result.match(new RegExp('{' + elementWithPrefix + '\\.[a-žA-Ž0-9-]+\\s*[?}\\|]', 'gm'))).filter(function (obj) { return obj; });
                        if (!subindexElementArray.length) {
                            _this.mokaParserInstance.templateVariables[elementWithPrefix] = val;
                        }
                        else {
                            subindexElementArray.forEach(function (el) {
                                subindex = el.split('.')[1].replace('}', '').replace('?', '').trim().split('|')[0];
                                var elementWithPrefixCopy = elementWithPrefix + '-' + subindex;
                                _this.mokaParserInstance.templateVariables[elementWithPrefixCopy] = val[subindex];
                                result = result.replace('.' + subindex, '-' + subindex);
                            });
                        }
                    });
                    _this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, result);
                }
            });
        };
        return MokaParserForeach;
    }());
    exports.MokaParserForeach = MokaParserForeach;
});

//# sourceMappingURL=mokaParserForeach.js.map
