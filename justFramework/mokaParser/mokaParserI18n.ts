import {MokaParser} from "justFramework/mokaParser/mokaParser";
import {MokaParserRegexNames} from "justFramework/mokaParser/mokaParserRegexNames";
import {i18n} from "justFramework/i18n";

export class MokaParserI18n {
	private objectArray = [];
	private mokaParserInstance: MokaParser;

	public get(mokaParserInstance: MokaParser): void {
		this.mokaParserInstance = mokaParserInstance;
		this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(new RegExp('{\\s*i18n\\s*[\'"]'+ MokaParserRegexNames.regexFunctionParam.source +'[\'"]\\s*}','gm'))).filter((obj) => obj);
		this.process();
	}

	private process(): void {
		let _this = this;
		let parsedBlock = {
			'value': null
		};
		this.objectArray.forEach((block) => {
			parsedBlock.value = block.replace(new RegExp('{.*?[\'"]', 'gm'), '').replace(new RegExp('[\'"]\\s*}', 'gm'), '');
			_this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, i18n.get(parsedBlock.value));
		});
	}
}

export class MokaParserI18nPage {
	private objectArray = [];
	private mokaParserInstance: MokaParser;

	public get(mokaParserInstance: MokaParser): void {
		this.mokaParserInstance = mokaParserInstance;
		this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(new RegExp('{\\s*i18n\\s*[\'"]'+ MokaParserRegexNames.regexFunctionParam.source +'[\'"]\\s*,\\s*[\'"]'+ MokaParserRegexNames.regexFunctionParam.source +'[\'"]\\s*}','gm'))).filter((obj) => obj);
		this.process();
	}

	private process(): void {
		let _this = this;
		let parsedBlock = {
			'value': null,
			'page': null
		};
		this.objectArray.forEach((block) => {
			parsedBlock.value = block.replace(new RegExp('{.*?[\'"]', 'gm'), '').replace(new RegExp('[\'"]\\s*,\\s*[\'"].*', 'gm'), '');
			parsedBlock.page = block.replace(new RegExp('.*[\'"]\\s*,\\s*[\'"]', 'gm'), '').replace(new RegExp('[\'"]\\s*}', 'gm'), '');
			_this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, i18n.get(parsedBlock.value, parsedBlock.page));
		});
	}
}