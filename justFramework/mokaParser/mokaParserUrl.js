(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "justFramework/mokaParser/mokaParserRegexNames", "justFramework/i18n"], factory);
    }
})(function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var mokaParserRegexNames_1 = require("justFramework/mokaParser/mokaParserRegexNames");
    var i18n_1 = require("justFramework/i18n");
    var MokaParserUrl = /** @class */ (function () {
        function MokaParserUrl() {
            this.objectArray = [];
        }
        MokaParserUrl.prototype.get = function (mokaParserInstance) {
            this.mokaParserInstance = mokaParserInstance;
            this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(new RegExp('{\\s*url\\s*[\'"]' + mokaParserRegexNames_1.MokaParserRegexNames.regexFunctionParam.source + '[\'"]\\s*}', 'gm'))).filter(function (obj) { return obj; });
            this.process();
        };
        MokaParserUrl.prototype.process = function () {
            var _this = this;
            var parsedBlock = {
                'page': null
            };
            this.objectArray.forEach(function (block) {
                parsedBlock.page = block.replace(new RegExp('{.*?[\'"]', 'gm'), '').replace(new RegExp('[\'"]\\s*}', 'gm'), '');
                _this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, '/' + i18n_1.i18n.get('page-url', parsedBlock.page));
            });
        };
        return MokaParserUrl;
    }());
    exports.MokaParserUrl = MokaParserUrl;
});

//# sourceMappingURL=mokaParserUrl.js.map
