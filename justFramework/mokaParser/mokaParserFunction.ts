import {MokaParser} from "justFramework/mokaParser/mokaParser";
import {MokaParserRegexNames} from "justFramework/mokaParser/mokaParserRegexNames";
import {Utils} from "justFramework/utils";

export class MokaParserFunction {
	private objectArray = [];
	private mokaParserInstance: MokaParser;

	public get(mokaParserInstance: MokaParser): void {
		this.mokaParserInstance = mokaParserInstance;
		this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(new RegExp('{\\s*' + MokaParserRegexNames.variableName.source + '\\s*\\|.*}', 'gm'))).filter((obj) => obj);
		this.process();
	}

	private process(): void {
		let _this = this;
		let variable = {
			name: null,
			property: null,
			value: null,
			function: null,
			firstParam: null,
			secondParam: null
		};
		this.objectArray.forEach((block) => {
			variable.name = block.replace(/{\s*/gm, '').replace(/\s*\|.*?}/gm, '');

			variable.property = MokaParserRegexNames.getPropertyFromVariableName(variable.name);
			if(variable.property)
				variable.name = variable.name.replace('.' + variable.property, '');			

			variable.value = _this.mokaParserInstance.templateVariables[variable.name];
			if(variable.value !== undefined) {
				if(variable.property)
					variable.value = variable.value[variable.property];

				let parts = block.split('|');
				parts.shift();
				parts.forEach(function (part) {
					part = part.replace('}', '').trim();
					if(new RegExp('^'+ MokaParserRegexNames.regexFunctionName.source +'$', 'gm').test(part)) {
						variable.function = part;
						variable.value = _this.processFunction(variable.function, variable.value);
					} else if(new RegExp(MokaParserRegexNames.regexFunctionName.source +'\\s*\\(\\s*[\'"]'+ MokaParserRegexNames.regexFunctionParam.source +'[\'"]\\s*\\)', 'gm').test(part)) {
						variable.function = part.replace(new RegExp('\\s*\\(\\s*["\']'+ MokaParserRegexNames.regexFunctionParam.source +'["\']\\s*\\)', 'gm'), '');
						variable.firstParam = part.replace(new RegExp(MokaParserRegexNames.regexFunctionName.source +'\\s*\\(\\s*["\']', 'gm'), '').replace(/'\s*\)/gm, '');
						variable.value = _this.processFunctionWithParam(variable.function, variable.value, variable.firstParam);
					} else if(new RegExp(MokaParserRegexNames.regexFunctionName.source +'\\s*\\(\\s*[\'"]'+ MokaParserRegexNames.regexFunctionParam.source +'[\'"]\\s*,\\s*[\'"]'+ MokaParserRegexNames.regexFunctionParam.source +'[\'"]\\s*\\)', 'gm').test(part)) {
						variable.function = part.replace(/\s*\(.*?\)/gm, '');
						variable.firstParam = part.replace(new RegExp(MokaParserRegexNames.regexFunctionName.source +'\\s*\\(\\s*["\']', 'gm'), '').replace(/['"]\s*,\s*['"].*?\)/gm, '');
						variable.secondParam = part.replace(/.*?['"]\s*,\s*['"]/gm, '').replace(/['"]\s*\)/gm, '');
						variable.value = _this.processFunctionWithTwoParams(variable.function, variable.value, variable.firstParam, variable.secondParam);
					}
				});
				_this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, variable.value);
			}
		});
	}

	private processFunction(functionName, variableValue) {
		switch(functionName) {
			case 'uppercase':
				variableValue = variableValue.toUpperCase();
				break;
			case 'lowercase':
				variableValue = variableValue.toLowerCase();
				break;
			case 'length':
				variableValue = variableValue.length;
				break;
			case 'trim':
				variableValue = $.trim(variableValue);
				break;
			case 'striptags':
				variableValue = $('<div/>').html(variableValue).text();
				break;
			case 'firstUpper':
				variableValue = variableValue[0].toUpperCase() + variableValue.slice(1);
				break;
			case 'capitalize':
				variableValue = Utils.capitalize(variableValue);
				break;
			case 'url':
				variableValue = Utils.stringToUrl(variableValue);
				break;
			case 'breakline':
				variableValue = variableValue.split("\n").join('<br />');
				break;

			default:
				throw new Error('Function "' + functionName + '" not found!');
		}
		return variableValue;
	}

	private processFunctionWithParam(functionName, variableValue, oneParam): any {
		switch(functionName) {
			case 'repeat':
				// @ts-ignore
				variableValue = variableValue.repeat(oneParam);
				break;
			case 'date':
				// @ts-ignore
				variableValue = new Date(variableValue * 1000).format(oneParam);
				break;

			default:
				throw new Error('Function "' + functionName + '" not found!');
		}
		return variableValue;
	}

	private processFunctionWithTwoParams(functionName, variableValue, oneParam, twoParam): any {
		switch(functionName) {
			case 'replace':
				variableValue = variableValue.replace(new RegExp(oneParam, 'gm'), twoParam);
				break;

			case 'truncate':
				variableValue = variableValue.substr(0, variableValue.lastIndexOf(' ', oneParam)) + twoParam;
				break;

			default:
				throw new Error('Function "' + functionName + '" not found!');
		}
		return variableValue;
	}
}