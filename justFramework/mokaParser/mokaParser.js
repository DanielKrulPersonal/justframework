(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "justFramework/mokaParser/mokaParserLoader", "justFramework/mokaParser/mokaParserComment", "justFramework/mokaParser/mokaParserSimpleVariable", "justFramework/mokaParser/mokaParserFunction", "justFramework/mokaParser/mokaParserInclude", "justFramework/mokaParser/mokaParserUrl", "justFramework/mokaParser/mokaParserConfig", "justFramework/mokaParser/mokaParserI18n", "justFramework/mokaParser/mokaParserTernary", "justFramework/mokaParser/mokaParserMeta", "justFramework/mokaParser/mokaParserForeach", "justFramework/mokaParser/mokaParserIsActive", "justFramework/mokaParser/mokaParserSetURLParam"], factory);
    }
})(function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var mokaParserLoader_1 = require("justFramework/mokaParser/mokaParserLoader");
    var mokaParserComment_1 = require("justFramework/mokaParser/mokaParserComment");
    var mokaParserSimpleVariable_1 = require("justFramework/mokaParser/mokaParserSimpleVariable");
    var mokaParserFunction_1 = require("justFramework/mokaParser/mokaParserFunction");
    var mokaParserInclude_1 = require("justFramework/mokaParser/mokaParserInclude");
    var mokaParserUrl_1 = require("justFramework/mokaParser/mokaParserUrl");
    var mokaParserConfig_1 = require("justFramework/mokaParser/mokaParserConfig");
    var mokaParserI18n_1 = require("justFramework/mokaParser/mokaParserI18n");
    var mokaParserTernary_1 = require("justFramework/mokaParser/mokaParserTernary");
    var mokaParserMeta_1 = require("justFramework/mokaParser/mokaParserMeta");
    var mokaParserForeach_1 = require("justFramework/mokaParser/mokaParserForeach");
    var mokaParserIsActive_1 = require("justFramework/mokaParser/mokaParserIsActive");
    var mokaParserSetURLParam_1 = require("justFramework/mokaParser/mokaParserSetURLParam");
    var MokaParser = /** @class */ (function () {
        /**
         * @param {string} templateLocation
         * @param {object} templateVariables
         */
        function MokaParser(templateLocation, templateVariables) {
            this.templateString = '';
            this.templateLocation = '';
            this.templateLocation = templateLocation;
            this.templateVariables = templateVariables;
            this.templateString = this.loadTemplate();
            this.processTemplate();
        }
        MokaParser.prototype.loadTemplate = function () {
            return mokaParserLoader_1.MokaParserLoader.getTemplateString(this.templateLocation);
        };
        MokaParser.prototype.processTemplate = function () {
            // check if there is some expressions
            var counter = 0;
            while (/{.*}/gm.test(this.templateString)) {
                if (counter === 20)
                    break;
                this.processTemplateVariables();
                this.processTemplateBlocks();
                counter++;
            }
        };
        MokaParser.prototype.processTemplateVariables = function () {
            new mokaParserComment_1.MokaParserComment().get(this);
            new mokaParserSimpleVariable_1.MokaParserSimpleVariable().get(this);
            new mokaParserFunction_1.MokaParserFunction().get(this);
        };
        MokaParser.prototype.processTemplateBlocks = function () {
            new mokaParserInclude_1.MokaParserInclude().get(this);
            new mokaParserInclude_1.MokaParserIncludePage().get(this);
            new mokaParserUrl_1.MokaParserUrl().get(this);
            new mokaParserConfig_1.MokaParserConfig().get(this);
            new mokaParserI18n_1.MokaParserI18n().get(this);
            new mokaParserI18n_1.MokaParserI18nPage().get(this);
            new mokaParserTernary_1.MokaParserShortTernary().get(this);
            new mokaParserTernary_1.MokaParserShortTernaryWithOperator().get(this);
            new mokaParserMeta_1.MokaParserMetaAuthor().get(this);
            new mokaParserMeta_1.MokaParserMetaDescription().get(this);
            new mokaParserMeta_1.MokaParserSubtitle().get(this);
            new mokaParserForeach_1.MokaParserForeach().get(this);
            new mokaParserIsActive_1.MokaParserIsActive().get(this);
            new mokaParserSetURLParam_1.MokaParserSetURLParam().get(this);
        };
        MokaParser.prototype.getOutput = function () {
            return this.templateString;
        };
        return MokaParser;
    }());
    exports.MokaParser = MokaParser;
});

//# sourceMappingURL=mokaParser.js.map
