(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "justFramework/mokaParser/mokaParserRegexNames", "justFramework/routes"], factory);
    }
})(function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var mokaParserRegexNames_1 = require("justFramework/mokaParser/mokaParserRegexNames");
    var routes_1 = require("justFramework/routes");
    var MokaParserIsActive = /** @class */ (function () {
        function MokaParserIsActive() {
            this.objectArray = [];
        }
        MokaParserIsActive.prototype.get = function (mokaParserInstance) {
            this.mokaParserInstance = mokaParserInstance;
            this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(new RegExp('{\\s*isActive\\s*[\'"]' + mokaParserRegexNames_1.MokaParserRegexNames.regexFunctionParam.source + '[\'"]\\s*}', 'gm'))).filter(function (obj) { return obj; });
            this.process();
        };
        MokaParserIsActive.prototype.process = function () {
            var _this = this;
            var parsedBlock = {
                'page': null
            };
            this.objectArray.forEach(function (block) {
                parsedBlock.page = block.replace(new RegExp('{.*?[\'"]', 'gm'), '').replace(new RegExp('[\'"]\\s*}', 'gm'), '');
                _this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, parsedBlock.page === routes_1.Routes.actualPath ? 'active' : '');
            });
        };
        return MokaParserIsActive;
    }());
    exports.MokaParserIsActive = MokaParserIsActive;
});

//# sourceMappingURL=mokaParserIsActive.js.map
