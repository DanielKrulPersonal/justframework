(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "justFramework/mokaParser/mokaParserRegexNames"], factory);
    }
})(function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var mokaParserRegexNames_1 = require("justFramework/mokaParser/mokaParserRegexNames");
    var MokaParserSetURLParam = /** @class */ (function () {
        function MokaParserSetURLParam() {
            this.objectArray = [];
        }
        MokaParserSetURLParam.prototype.get = function (mokaParserInstance) {
            this.mokaParserInstance = mokaParserInstance;
            this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(new RegExp('{\\s*setURLParam\\s*[\'"]' + mokaParserRegexNames_1.MokaParserRegexNames.regexFunctionParam.source + '[\'"]\\s*,\\s*[\'"]' + mokaParserRegexNames_1.MokaParserRegexNames.regexFunctionParam.source + '[\'"]\\s*}', 'gm'))).filter(function (obj) { return obj; });
            this.process();
        };
        MokaParserSetURLParam.prototype.process = function () {
            var _this = this;
            var parsedBlock = {
                'name': null,
                'value': null
            };
            this.objectArray.forEach(function (block) {
                parsedBlock.name = block.replace(new RegExp('{.*?[\'"]', 'gm'), '').replace(new RegExp('[\'"]\\s*,\\s*[\'"].*', 'gm'), '');
                parsedBlock.value = block.replace(new RegExp('.*[\'"]\\s*,\\s*[\'"]', 'gm'), '').replace(new RegExp('[\'"]\\s*}', 'gm'), '');
                var urlParams = new URLSearchParams(window.location.search);
                urlParams.set(parsedBlock.name, parsedBlock.value);
                _this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, urlParams.toString());
            });
        };
        return MokaParserSetURLParam;
    }());
    exports.MokaParserSetURLParam = MokaParserSetURLParam;
});

//# sourceMappingURL=mokaParserSetURLParam.js.map
