import {MokaParser} from "justFramework/mokaParser/mokaParser";

export class MokaParserComment {
	private objectArray = [];
	private mokaParserInstance: MokaParser;

	public get(mokaParserInstance: MokaParser): void {
		this.mokaParserInstance = mokaParserInstance;
		this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(/{\*[\s\S]*?\*}/gm)).filter((obj) => obj);
		this.process();
	}

	private process(): void {
		let _this = this;
		this.objectArray.forEach((block) => {
			_this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, '');
		});
	}
}