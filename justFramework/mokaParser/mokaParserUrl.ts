import {MokaParser} from "justFramework/mokaParser/mokaParser";
import {MokaParserRegexNames} from "justFramework/mokaParser/mokaParserRegexNames";
import {i18n} from "justFramework/i18n";

export class MokaParserUrl {
	private objectArray = [];
	private mokaParserInstance: MokaParser;

	public get(mokaParserInstance: MokaParser): void {
		this.mokaParserInstance = mokaParserInstance;
		this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(new RegExp('{\\s*url\\s*[\'"]'+ MokaParserRegexNames.regexFunctionParam.source +'[\'"]\\s*}','gm'))).filter((obj) => obj);
		this.process();
	}

	private process(): void {
		let _this = this;
		let parsedBlock = {
			'page': null
		};
		this.objectArray.forEach((block) => {
			parsedBlock.page = block.replace(new RegExp('{.*?[\'"]', 'gm'), '').replace(new RegExp('[\'"]\\s*}', 'gm'), '');
			_this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, '/' + i18n.get('page-url', parsedBlock.page));
		});
	}
}