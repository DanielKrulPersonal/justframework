import {MokaParserLoader} from "justFramework/mokaParser/mokaParserLoader";
import {MokaParserComment} from "justFramework/mokaParser/mokaParserComment";
import {MokaParserSimpleVariable} from "justFramework/mokaParser/mokaParserSimpleVariable";
import {MokaParserFunction} from "justFramework/mokaParser/mokaParserFunction";
import {MokaParserInclude, MokaParserIncludePage} from "justFramework/mokaParser/mokaParserInclude";
import {MokaParserUrl} from "justFramework/mokaParser/mokaParserUrl";
import {MokaParserConfig} from "justFramework/mokaParser/mokaParserConfig";
import {MokaParserI18n, MokaParserI18nPage} from "justFramework/mokaParser/mokaParserI18n";
import {MokaParserShortTernaryWithOperator, MokaParserShortTernary} from "justFramework/mokaParser/mokaParserTernary";
import {MokaParserMetaAuthor, MokaParserMetaDescription, MokaParserSubtitle} from "justFramework/mokaParser/mokaParserMeta";
import {MokaParserForeach} from "justFramework/mokaParser/mokaParserForeach";
import {MokaParserIsActive} from "justFramework/mokaParser/mokaParserIsActive";
import {MokaParserSetURLParam} from "justFramework/mokaParser/mokaParserSetURLParam";

export class MokaParser {
	public templateVariables: object;
	public templateString: string = '';
	private readonly templateLocation: string = '';

	/**
	 * @param {string} templateLocation
	 * @param {object} templateVariables
	 */
	public constructor(templateLocation: string, templateVariables: object) {
		this.templateLocation = templateLocation;
		this.templateVariables = templateVariables;
		this.templateString = this.loadTemplate();
		this.processTemplate();
	}

	private loadTemplate(): string {
		return MokaParserLoader.getTemplateString(this.templateLocation);
	}

	private processTemplate(): void {
		// check if there is some expressions
		let counter = 0;
		while (/{.*}/gm.test(this.templateString)) {
			if(counter === 20) break;

			this.processTemplateVariables();
			this.processTemplateBlocks();

			counter++;
		}
	}

	private processTemplateVariables(): void {
		new MokaParserComment().get(this);
		new MokaParserSimpleVariable().get(this);
		new MokaParserFunction().get(this);
	}

	private processTemplateBlocks(): void {
		new MokaParserInclude().get(this);
		new MokaParserIncludePage().get(this);
		new MokaParserUrl().get(this);
		new MokaParserConfig().get(this);
		new MokaParserI18n().get(this);
		new MokaParserI18nPage().get(this);
		new MokaParserShortTernary().get(this);
		new MokaParserShortTernaryWithOperator().get(this);
		new MokaParserMetaAuthor().get(this);
		new MokaParserMetaDescription().get(this);
		new MokaParserSubtitle().get(this);
		new MokaParserForeach().get(this);
		new MokaParserIsActive().get(this);
		new MokaParserSetURLParam().get(this);
	}

	public getOutput(): string {
		return this.templateString;
	}
}