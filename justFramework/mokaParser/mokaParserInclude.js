(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "justFramework/mokaParser/mokaParserRegexNames", "justFramework/routes", "justFramework/mokaParser/mokaParserLoader"], factory);
    }
})(function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var mokaParserRegexNames_1 = require("justFramework/mokaParser/mokaParserRegexNames");
    var routes_1 = require("justFramework/routes");
    var mokaParserLoader_1 = require("justFramework/mokaParser/mokaParserLoader");
    var MokaParserInclude = /** @class */ (function () {
        function MokaParserInclude() {
            this.objectArray = [];
        }
        MokaParserInclude.prototype.get = function (mokaParserInstance) {
            this.mokaParserInstance = mokaParserInstance;
            this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(new RegExp('{\\s*include\\s*[\'"]' + mokaParserRegexNames_1.MokaParserRegexNames.regexFunctionParam.source + '[\'"]\\s*}', 'gm'))).filter(function (obj) { return obj; });
            this.process();
        };
        MokaParserInclude.prototype.process = function () {
            var _this = this;
            var parsedBlock = {
                'templateName': null
            };
            this.objectArray.forEach(function (block) {
                parsedBlock.templateName = block.replace(new RegExp('{.*?[\'"]', 'gm'), '').replace(new RegExp('[\'"]\\s*}', 'gm'), '');
                _this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, mokaParserLoader_1.MokaParserLoader.getTemplateString('/pages/' + routes_1.Routes.actualPath + '/views/' + parsedBlock.templateName + '.moka'));
            });
        };
        return MokaParserInclude;
    }());
    exports.MokaParserInclude = MokaParserInclude;
    var MokaParserIncludePage = /** @class */ (function () {
        function MokaParserIncludePage() {
            this.objectArray = [];
        }
        MokaParserIncludePage.prototype.get = function (mokaParserInstance) {
            this.mokaParserInstance = mokaParserInstance;
            this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(new RegExp('{\\s*include\\s*[\'"]' + mokaParserRegexNames_1.MokaParserRegexNames.regexFunctionParam.source + '[\'"]\\s*,\\s*[\'"]' + mokaParserRegexNames_1.MokaParserRegexNames.regexFunctionParam.source + '[\'"]\\s*}', 'gm'))).filter(function (obj) { return obj; });
            this.process();
        };
        MokaParserIncludePage.prototype.process = function () {
            var _this = this;
            var parsedBlock = {
                'templateName': null,
                'page': null
            };
            this.objectArray.forEach(function (block) {
                parsedBlock.templateName = block.replace(new RegExp('{.*?[\'"]', 'gm'), '').replace(new RegExp('[\'"]\\s*,\\s*[\'"].*', 'gm'), '');
                parsedBlock.page = block.replace(new RegExp('.*[\'"]\\s*,\\s*[\'"]', 'gm'), '').replace(new RegExp('[\'"]\\s*}', 'gm'), '');
                _this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, mokaParserLoader_1.MokaParserLoader.getTemplateString('/pages/' + parsedBlock.page + '/views/' + parsedBlock.templateName + '.moka'));
            });
        };
        return MokaParserIncludePage;
    }());
    exports.MokaParserIncludePage = MokaParserIncludePage;
});

//# sourceMappingURL=mokaParserInclude.js.map
