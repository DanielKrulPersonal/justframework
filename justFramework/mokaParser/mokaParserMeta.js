(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "justFramework/mokaParser/mokaParserRegexNames"], factory);
    }
})(function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var mokaParserRegexNames_1 = require("justFramework/mokaParser/mokaParserRegexNames");
    var MokaParserMetaAuthor = /** @class */ (function () {
        function MokaParserMetaAuthor() {
            this.objectArray = [];
        }
        MokaParserMetaAuthor.prototype.get = function (mokaParserInstance) {
            this.mokaParserInstance = mokaParserInstance;
            this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(new RegExp('{\\s*meta-author\\s*[\'"]' + mokaParserRegexNames_1.MokaParserRegexNames.regexFunctionParam.source + '[\'"]\\s*}', 'gm'))).filter(function (obj) { return obj; });
            this.process();
        };
        MokaParserMetaAuthor.prototype.process = function () {
            var _this = this;
            var parsedBlock = {
                'value': null
            };
            this.objectArray.forEach(function (block) {
                parsedBlock.value = block.replace(new RegExp('{.*?[\'"]', 'gm'), '').replace(new RegExp('[\'"]\\s*}', 'gm'), '');
                $('meta[name=author]').attr('content', parsedBlock.value);
                _this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, '');
            });
        };
        return MokaParserMetaAuthor;
    }());
    exports.MokaParserMetaAuthor = MokaParserMetaAuthor;
    var MokaParserMetaDescription = /** @class */ (function () {
        function MokaParserMetaDescription() {
            this.objectArray = [];
        }
        MokaParserMetaDescription.prototype.get = function (mokaParserInstance) {
            this.mokaParserInstance = mokaParserInstance;
            this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(new RegExp('{\\s*meta-description\\s*[\'"]' + mokaParserRegexNames_1.MokaParserRegexNames.regexFunctionParam.source + '[\'"]\\s*}', 'gm'))).filter(function (obj) { return obj; });
            this.process();
        };
        MokaParserMetaDescription.prototype.process = function () {
            var _this = this;
            var parsedBlock = {
                'value': null
            };
            this.objectArray.forEach(function (block) {
                parsedBlock.value = block.replace(new RegExp('{.*?[\'"]', 'gm'), '').replace(new RegExp('[\'"]\\s*}', 'gm'), '');
                $('meta[name=description]').attr('content', parsedBlock.value);
                _this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, '');
            });
        };
        return MokaParserMetaDescription;
    }());
    exports.MokaParserMetaDescription = MokaParserMetaDescription;
    var MokaParserSubtitle = /** @class */ (function () {
        function MokaParserSubtitle() {
            this.objectArray = [];
        }
        MokaParserSubtitle.prototype.get = function (mokaParserInstance) {
            this.mokaParserInstance = mokaParserInstance;
            this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(new RegExp('{\\s*subtitle\\s*[\'"]' + mokaParserRegexNames_1.MokaParserRegexNames.regexFunctionParam.source + '[\'"]\\s*}', 'gm'))).filter(function (obj) { return obj; });
            this.process();
        };
        MokaParserSubtitle.prototype.process = function () {
            var _this = this;
            var parsedBlock = {
                'value': null
            };
            this.objectArray.forEach(function (block) {
                parsedBlock.value = block.replace(new RegExp('{.*?[\'"]', 'gm'), '').replace(new RegExp('[\'"]\\s*}', 'gm'), '');
                if (parsedBlock.value.trim() !== '')
                    $(document).attr('title', parsedBlock.value + ' | ' + $(document).attr('title'));
                _this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, '');
            });
        };
        return MokaParserSubtitle;
    }());
    exports.MokaParserSubtitle = MokaParserSubtitle;
});

//# sourceMappingURL=mokaParserMeta.js.map
