import {MokaParser} from "justFramework/mokaParser/mokaParser";
import {MokaParserRegexNames} from "justFramework/mokaParser/mokaParserRegexNames";

export class MokaParserForeach {
	private objectArray = [];
	private mokaParserInstance: MokaParser;

	public get(mokaParserInstance: MokaParser): void {
		this.mokaParserInstance = mokaParserInstance;
		this.objectArray = this.objectArray.concat(this.mokaParserInstance.templateString.match(new RegExp('{\\s*foreach\\s*'+ MokaParserRegexNames.variableName.source +'\\s*as\\s*'+ MokaParserRegexNames.variableName.source +'\\s*}[\\s\\S]*?{\\s*\\/\\s*foreach\\s*}', 'gm'))).filter((obj) => obj);
		this.process();
	}

	private process(): void {
		let _this = this;
		let parsedBlock = {
			'variable': null,
			'property': null,
			'element': null,
			'content': null,
			'value': null
		};
		this.objectArray.forEach((block) => {
			parsedBlock.variable = block.replace(/{\s*foreach\s*/gm, '').replace(new RegExp('\\s*as\\s*'+ MokaParserRegexNames.variableName.source +'\\s*}[\\s\\S]*?{\\s*\\/\\s*foreach\\s*}', 'gm'), '');
			parsedBlock.property = MokaParserRegexNames.getPropertyFromVariableName(parsedBlock.variable);
			if(parsedBlock.property)
				parsedBlock.variable = parsedBlock.variable.replace('.' + parsedBlock.property, '');

			parsedBlock.element = block.replace(new RegExp('{\\s*foreach\\s*'+ MokaParserRegexNames.variableName.source +'\\s*as\\s*', 'gm'), '').replace(/}[\s\S]*?{\s*\/\s*foreach\s*}/gm, '');
			parsedBlock.content = block.replace(new RegExp('{\\s*foreach\\s*'+ MokaParserRegexNames.variableName.source +'\\s*as\\s*'+ MokaParserRegexNames.variableName.source +'\\s*}\\n*\\t*', 'gm'), '').replace(/\n*\t*{\s*\/\s*foreach\s*}/gm, '');
			let result = '';

			if(_this.mokaParserInstance.templateVariables[parsedBlock.variable] !== undefined) {
				// variable value is array or object
				parsedBlock.value = _this.mokaParserInstance.templateVariables[parsedBlock.variable];
				if(parsedBlock.property)
					parsedBlock.value = parsedBlock.value[parsedBlock.property];

				if(typeof parsedBlock.value !== 'object') throw new Error('Variable "'+ parsedBlock.variable +'" is not array!');

				parsedBlock.value.forEach(function (val, index) {
					let elementWithPrefix = parsedBlock.element + '-foreach-' + index;
					result += parsedBlock.content.replace(new RegExp('{\\s*' + parsedBlock.element, 'gm'), '{' + elementWithPrefix);

					let subindex = null;
					let subindexElementArray = [].concat(result.match(new RegExp('{'+ elementWithPrefix +'\\.[a-žA-Ž0-9-]+\\s*[?}\\|]', 'gm'))).filter((obj) => obj);
					if(!subindexElementArray.length) {
						_this.mokaParserInstance.templateVariables[elementWithPrefix] = val;
					} else {
						subindexElementArray.forEach(function (el) {
							subindex = el.split('.')[1].replace('}', '').replace('?', '').trim().split('|')[0];
							let elementWithPrefixCopy = elementWithPrefix + '-' + subindex;
							_this.mokaParserInstance.templateVariables[elementWithPrefixCopy] = val[subindex];
							result = result.replace('.' + subindex, '-' + subindex);
						});
					}
				});
				_this.mokaParserInstance.templateString = _this.mokaParserInstance.templateString.replace(block, result);
			}
		});
	}
}