export class MokaParserRegexNames {
	static variableName: RegExp = /(?:(?![{}|])[a-žA-Ž0-9-_.])+/;
	static regexFunctionName: RegExp = /[a-žA-Ž0-9-_]+/;
	static regexFunctionParam: RegExp = /(?:(?![{}|])[a-žA-Ž0-9 &#\/'".:-])+/;

	/**
	* @param {string} variableName
	* @returns {string}
	*/
	static getPropertyFromVariableName(variableName: string): string {
		let parts = variableName.split('.');
		if(parts.length === 1) return null;

		return parts[1].split('|')[0];
	}
}