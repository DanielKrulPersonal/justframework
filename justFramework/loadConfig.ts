import {Config} from "justFramework/config";

// check jQuery
if(!window['jQuery']) {
	throw new Error('jQuery not found!');
}
(function fallback() {
	if(typeof URLSearchParams == "undefined") {
		$('head').append('<script type="text/javascript" defer src="/justFramework/libs/url-search-params.js"></script>');
	}
})();
(function loadConfig() {
	$.ajax({
		url: '/config.json',
		dataType: 'json',
		cache: false,
		async: false,
		success: function(json){
			Object.keys(json).forEach(function(key) {
				Config.set(key, json[key]);
			});
		}
	});
})();
if(!Config.get('cache')) {
	requirejs.config({
		urlArgs: 'v=' + (+new Date).toString()
	});
}