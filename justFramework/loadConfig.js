(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "justFramework/config"], factory);
    }
})(function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var config_1 = require("justFramework/config");
    // check jQuery
    if (!window['jQuery']) {
        throw new Error('jQuery not found!');
    }
    (function fallback() {
        if (typeof URLSearchParams == "undefined") {
            $('head').append('<script type="text/javascript" defer src="/justFramework/libs/url-search-params.js"></script>');
        }
    })();
    (function loadConfig() {
        $.ajax({
            url: '/config.json',
            dataType: 'json',
            cache: false,
            async: false,
            success: function (json) {
                Object.keys(json).forEach(function (key) {
                    config_1.Config.set(key, json[key]);
                });
            }
        });
    })();
    if (!config_1.Config.get('cache')) {
        requirejs.config({
            urlArgs: 'v=' + (+new Date).toString()
        });
    }
});

//# sourceMappingURL=loadConfig.js.map
