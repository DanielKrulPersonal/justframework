import {expect} from 'chai';
import {MokaParser} from "justFramework/mokaParser/mokaParser";
let sinon = require('sinon');
declare var global: any;
global.$ = global.jQuery = require('jquery');

describe('MokaParser should return', () => {
	let templateString = `
	{testVariable}
	{testVariable|uppercase}
	{testVariable|lowercase}
	{testVariable|length}
	{testVariable|trim}
	{testVariable|striptags}
	{testVariable|firstUpper}
	{testVariable|capitalize}
	{testVariable|url}
	{testVariable|url|uppercase}
	`;
	let templateVariables = {
		'testVariable': 'Jaffa kree!<br>'
	};
	let templateResult = `
	Jaffa kree!<br>
	JAFFA KREE!<BR>
	jaffa kree!<br>
	15
	Jaffa kree!<br>
	Jaffa kree!
	Jaffa kree!<br>
	Jaffa Kree!<br>
	jaffa-kree-br
	JAFFA-KREE-BR
	`;

	sinon.stub(MokaParser.prototype, <any>'loadTemplate').returns(templateString);

	let mokaInstance: MokaParser = new MokaParser('', templateVariables);
	it('"' + templateResult + '"' + ' when ' + '"' + templateString + '"', function() {
		expect(mokaInstance.getOutput()).to.equal(templateResult);
	});
});