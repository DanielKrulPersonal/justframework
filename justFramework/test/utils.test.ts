import {expect} from 'chai';
import {Utils} from "../utils";

describe('Utils should return', () => {
	it('"Test String" when Utils.capitalize("test string")', function() {
		expect(Utils.capitalize('test string')).to.equal('Test String');
	});
	it('"toto-je-nejaky-utf8-string" when Utils.stringToUrl("toto je nějaký UTF8 string")', function() {
		expect(Utils.stringToUrl('toto je nějaký UTF8 string')).to.equal('toto-je-nejaky-utf8-string');
	});
});