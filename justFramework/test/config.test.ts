import {expect} from 'chai';
import {Config} from 'justFramework/config';

Config.set('Test', 'setting');

describe('Config should return', () => {
	it('"setting" when Config.get("Test")', function() {
		expect(Config.get('Test')).to.equal('setting');
	});
});