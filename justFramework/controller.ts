import {Routes} from "justFramework/routes";
import {MokaParser} from "justFramework/mokaParser/mokaParser";

export abstract class Controller {
	public showMainTemplate: boolean = true;
	public templateLoadFadeIn: boolean = false;

	static register(): void {
		let className = this.toString().match(/^function\s*([^\s(]+)/)[1];
		window[className] = this;
	}

	protected getModelData(): any {
		return window[Routes.actualPath + 'ModelInstance'].data;
	}

	/**
	 * @param {string} methodName
	 * @param {function} callback
	 * @param {object} param
	 */
	protected getModelMethod(methodName: string, callback, param: object): void {
		let data = window[Routes.actualPath + 'ModelInstance'][methodName](param);
		callback(data);
	}

	/**
	 * @param {object} variables
	 */
	protected loadMainTemplate(variables: object = {}): void {
		// here we have to load a main template
		if(this.showMainTemplate) this.loadTemplate(variables, Routes.actualPath, Routes.actualPath);
	}

	/**
	 * @param {object} variables
	 * @param {string} templateName
	 * @param {string} page
	 */
	protected loadTemplate(variables: object = {}, templateName: string, page: string): void {
		let templateLocation: string = '/pages/' + page + '/views/' + templateName + '.moka';
		let mokaInstance = new MokaParser(templateLocation, variables);

		if(this.templateLoadFadeIn)
			$('body').hide().html(mokaInstance.getOutput()).fadeIn(250);
		else
			$('body').html(mokaInstance.getOutput());
	}

	/**
	 * @param {string} subPage
	 * @returns {string|number}
	 */
	protected getSubPageId(subPage: string): string|number  {
		return subPage.replace(/-.*/gm, '');
	}
	abstract initialize();
	abstract runEvent();
	abstract setRoutes(subPage: string);
}