let gulp = require('gulp');
let sourcemaps = require('gulp-sourcemaps');
let ts = require('gulp-typescript');
let less = require('gulp-less');
let tsconfig = require('./tsconfig.json');
let locations = {
	'core': 'justFramework/*.ts',
	'pages': 'pages/**/*.ts',
	'tools': 'justFramework/tools/*.ts',
	'mokaParser': 'justFramework/mokaParser/*.ts',
	'less-assets': 'assets/css/*.less'
};

// build for the core
gulp.task('build-ts-core', function () {
	return gulp.src(locations.core, {base: './'})
		.pipe(sourcemaps.init())
		.pipe(ts(tsconfig.compilerOptions))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('.'));
});

// build for the pages
gulp.task('build-ts-pages', function () {
	return gulp.src(locations.pages, {base: './'})
		.pipe(sourcemaps.init())
		.pipe(ts(tsconfig.compilerOptions))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('.'));
});

// build for the tools
gulp.task('build-ts-tools', function () {
	return gulp.src(locations.tools, {base: './'})
		.pipe(sourcemaps.init())
		.pipe(ts(tsconfig.compilerOptions))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('.'));
});

// build for the mokaParser
gulp.task('build-ts-mokaParser', function () {
	return gulp.src(locations.mokaParser, {base: './'})
		.pipe(sourcemaps.init())
		.pipe(ts(tsconfig.compilerOptions))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('.'));
});

// build for the less-assets
gulp.task('build-less-assets', function () {
	return gulp.src(locations['less-assets'], {base: './'})
		.pipe(sourcemaps.init())
		.pipe(less({
			'compress': true
		}))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('.'));
});

// watch the files for changes and rebuild everything
gulp.task('default', function() {
	gulp.watch(locations.core, {usePolling: true}, gulp.series('build-ts-core'));
	gulp.watch(locations.pages, {usePolling: true}, gulp.series('build-ts-pages'));
	gulp.watch(locations.tools, {usePolling: true}, gulp.series('build-ts-tools'));
	gulp.watch(locations.mokaParser, {usePolling: true}, gulp.series('build-ts-mokaParser'));
	gulp.watch(locations['less-assets'], {usePolling: true}, gulp.series('build-less-assets'));
});
gulp.task('compile', gulp.series('build-ts-core', 'build-ts-pages', 'build-ts-tools', 'build-ts-mokaParser', 'build-less-assets'));