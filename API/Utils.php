<?php
class Utils {
	public static function request($key, $value = null) {
		if (isset($_REQUEST[$key]))
			return $_REQUEST[$key];
		else if (isset($_REQUEST[$key = strtr($key, '.', '_')]))
			return $_REQUEST[$key];
		else
			return $value;
	}

	public static function getUniqueMd5Token() {
		return md5(uniqid(rand(), true));
	}

	public static function htmlescape($string) {
		return htmlspecialchars($string, ENT_COMPAT, 'utf-8');
	}

	public static function randomString($length, $chars = 'abcdefghijklmnopqrstuvwxyz0123456789') {
		$result = '';
		$max = strlen($chars) - 1;
		for ($i = 0; $i < $length; $i++)
			$result .= substr($chars, mt_rand(0, $max), 1);
		return $result;
	}
}