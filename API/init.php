<?php
require_once 'config.php';
require_once 'vendor/autoload.php';
require_once 'Response.php';
require_once 'Utils.php';

header('Access-Control-Allow-Origin: *');

class Database
{
	/** @var Dibi\Connection */
	public static $db;
	public static function initialize()
	{
		$dibiConfig = [
			'driver' => $GLOBALS['db']['driver'],
			'host' => $GLOBALS['db']['host'],
			'username' => $GLOBALS['db']['username'],
			'database' => $GLOBALS['db']['database'],
			'password' => $GLOBALS['db']['password'],
			'port' => $GLOBALS['db']['port']
		];
		self::$db = new Dibi\Connection($dibiConfig);
	}
}
Database::initialize();