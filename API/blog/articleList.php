<?php
require_once '../init.php';

class ArticleListResponse extends Response {
	public $data = [];
	public $numOfArticles = 0;

	public function __construct () {

		$this->getData();
		return $this->data;
	}

	private function getData() {
		$data = Database::$db->fetchAll("SELECT id, title, text, perex, image FROM blog_article");
		$this->numOfArticles = Database::$db->fetchSingle("SELECT COUNT(*) FROM blog_article");
		$this->data = $data;
	}
}

echo new ArticleListResponse();
