<?php
require_once '../init.php';

class ArticleDetailResponse extends Response {
	public $data = [];
	private $id;

	public function __construct ($id) {
		$this->id = $id;

		$this->getData();
		return $this->data;
	}

	private function getData() {
		$data = Database::$db->fetch("SELECT title, text, perex, image FROM blog_article WHERE id = ?", $this->id);
		$this->data = $data;
	}
}

echo new ArticleDetailResponse(Utils::request('id'));
