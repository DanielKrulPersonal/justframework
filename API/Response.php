<?php
class Response {
	public $errorMessage;
	protected $statusCode = 200;

	public function __toString()
	{
		http_response_code($this->statusCode);
		return json_encode($this);
	}
}