import {Model} from "justFramework/model";

class docsModel extends Model {
	public runEvent() {
		// zde se získávají data
		this.data = {
			'sirka': 24,
			'delka': 10
		};
	}
}
docsModel.register();