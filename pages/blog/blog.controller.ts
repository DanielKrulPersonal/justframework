import {Controller} from "justFramework/controller";
import {i18n} from "justFramework/i18n";
import {ImageUtilities} from "justFramework/tools/imageUtilities";
import {Pagination} from "justFramework/tools/pagination";
import {Breadcrumbs} from "justFramework/tools/breadcrumbs";

class blogController extends Controller {
	public initialize() {
		this.templateLoadFadeIn = true;
	}
	public runEvent() {
		//let data = super.getModelData();
	}
	public setRoutes(subPage: string): void {
		let breadcrumbsInstance: Breadcrumbs;

		// example: 32-article-name
		if(/\d+-[a-z0-9-]+/gm.test(subPage)) {
			let articleDetail: any = {};
			super.getModelMethod('getArticleDetail', function (data) {
				articleDetail = data.articleDetail;
			}, {
				'id': super.getSubPageId(subPage)
			});
			i18n.set('page-title', articleDetail.title);
			// @ts-ignore
			articleDetail.text = new showdown.Converter().makeHtml(articleDetail.text);

			breadcrumbsInstance = new Breadcrumbs();
			breadcrumbsInstance
				.add(i18n.get('home', 'index'), '/')
				.add(i18n.get('page-name', 'blog'), '/' + i18n.get('page-url', 'blog'))
				.add(articleDetail.title, '')
			;

			super.loadTemplate({
				'title': articleDetail.title,
				'text': articleDetail.text,
				'breadcrumbs': breadcrumbsInstance.get()
			}, 'detail', 'blog');
			this.showMainTemplate = false;
		} else {
			// index
			this.showMainTemplate = true;
			let articleList = [],
				numOfArticles: number = 0;
			super.getModelMethod('getArticleList', function (data) {
				articleList = data.articleList;
				numOfArticles = data.numOfArticles;
			}, {});

			let paginationInstance = new Pagination('page', numOfArticles, 7);
			super.loadMainTemplate({
				'articleList': articleList,
				'pagination': paginationInstance.get()
			});
			ImageUtilities.enableLazyLoad().cropInDOM(ImageUtilities.POSITION_CENTER, {width: 350, height: 150});
		}
	}
}
blogController.register();