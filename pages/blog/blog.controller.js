var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "justFramework/controller", "justFramework/i18n", "justFramework/tools/imageUtilities", "justFramework/tools/pagination", "justFramework/tools/breadcrumbs"], factory);
    }
})(function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var controller_1 = require("justFramework/controller");
    var i18n_1 = require("justFramework/i18n");
    var imageUtilities_1 = require("justFramework/tools/imageUtilities");
    var pagination_1 = require("justFramework/tools/pagination");
    var breadcrumbs_1 = require("justFramework/tools/breadcrumbs");
    var blogController = /** @class */ (function (_super) {
        __extends(blogController, _super);
        function blogController() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        blogController.prototype.initialize = function () {
            this.templateLoadFadeIn = true;
        };
        blogController.prototype.runEvent = function () {
            //let data = super.getModelData();
        };
        blogController.prototype.setRoutes = function (subPage) {
            var breadcrumbsInstance;
            // example: 32-article-name
            if (/\d+-[a-z0-9-]+/gm.test(subPage)) {
                var articleDetail_1 = {};
                _super.prototype.getModelMethod.call(this, 'getArticleDetail', function (data) {
                    articleDetail_1 = data.articleDetail;
                }, {
                    'id': _super.prototype.getSubPageId.call(this, subPage)
                });
                i18n_1.i18n.set('page-title', articleDetail_1.title);
                // @ts-ignore
                articleDetail_1.text = new showdown.Converter().makeHtml(articleDetail_1.text);
                breadcrumbsInstance = new breadcrumbs_1.Breadcrumbs();
                breadcrumbsInstance
                    .add(i18n_1.i18n.get('home', 'index'), '/')
                    .add(i18n_1.i18n.get('page-name', 'blog'), '/' + i18n_1.i18n.get('page-url', 'blog'))
                    .add(articleDetail_1.title, '');
                _super.prototype.loadTemplate.call(this, {
                    'title': articleDetail_1.title,
                    'text': articleDetail_1.text,
                    'breadcrumbs': breadcrumbsInstance.get()
                }, 'detail', 'blog');
                this.showMainTemplate = false;
            }
            else {
                // index
                this.showMainTemplate = true;
                var articleList_1 = [], numOfArticles_1 = 0;
                _super.prototype.getModelMethod.call(this, 'getArticleList', function (data) {
                    articleList_1 = data.articleList;
                    numOfArticles_1 = data.numOfArticles;
                }, {});
                var paginationInstance = new pagination_1.Pagination('page', numOfArticles_1, 7);
                _super.prototype.loadMainTemplate.call(this, {
                    'articleList': articleList_1,
                    'pagination': paginationInstance.get()
                });
                imageUtilities_1.ImageUtilities.enableLazyLoad().cropInDOM(imageUtilities_1.ImageUtilities.POSITION_CENTER, { width: 350, height: 150 });
            }
        };
        return blogController;
    }(controller_1.Controller));
    blogController.register();
});

//# sourceMappingURL=blog.controller.js.map
