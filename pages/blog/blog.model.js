var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "justFramework/model"], factory);
    }
})(function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var model_1 = require("justFramework/model");
    var blogModel = /** @class */ (function (_super) {
        __extends(blogModel, _super);
        function blogModel() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        blogModel.prototype.runEvent = function () {
            this.data = {};
        };
        blogModel.prototype.getArticleList = function (params) {
            var articleList = [], numOfArticles = 0;
            $.ajax({
                url: '/API/blog/articleList',
                method: 'POST',
                dataType: 'json',
                async: false,
                cache: false,
                success: function (response) {
                    articleList = response.data;
                    numOfArticles = response.numOfArticles;
                },
                statusCode: {}
            });
            return { 'articleList': articleList, 'numOfArticles': numOfArticles };
        };
        blogModel.prototype.getArticleDetail = function (params) {
            var articleDetail = {};
            $.ajax({
                url: '/API/blog/articleDetail',
                method: 'POST',
                data: {
                    'id': params['id']
                },
                dataType: 'json',
                async: false,
                cache: false,
                success: function (response) {
                    articleDetail = response.data;
                },
                statusCode: {}
            });
            return { 'articleDetail': articleDetail };
        };
        return blogModel;
    }(model_1.Model));
    blogModel.register();
});

//# sourceMappingURL=blog.model.js.map
