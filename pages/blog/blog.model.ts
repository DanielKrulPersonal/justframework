import {Model} from "justFramework/model";

class blogModel extends Model {
	public runEvent(): void{
		this.data = {
		};
	}

	public getArticleList(params: object) {
		let articleList = [], numOfArticles = 0;
		$.ajax({
			url: '/API/blog/articleList',
			method: 'POST',
			dataType: 'json',
			async: false,
			cache: false,
			success: function(response){
				articleList = response.data;
				numOfArticles = response.numOfArticles;
			},
			statusCode: {
			}
		});
		return {'articleList': articleList, 'numOfArticles': numOfArticles};
	}

	public getArticleDetail(params: object) {
		let articleDetail = {};
		$.ajax({
			url: '/API/blog/articleDetail',
			method: 'POST',
			data: {
				'id': params['id']
			},
			dataType: 'json',
			async: false,
			cache: false,
			success: function(response){
				articleDetail = response.data;
			},
			statusCode: {
			}
		});
		return {'articleDetail': articleDetail};
	}
}
blogModel.register();