var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "justFramework/controller"], factory);
    }
})(function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var controller_1 = require("justFramework/controller");
    var indexController = /** @class */ (function (_super) {
        __extends(indexController, _super);
        function indexController() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        indexController.prototype.initialize = function () {
            this.templateLoadFadeIn = true;
        };
        indexController.prototype.runEvent = function () {
            _super.prototype.loadMainTemplate.call(this, {});
        };
        indexController.prototype.setRoutes = function (subPage) {
        };
        return indexController;
    }(controller_1.Controller));
    indexController.register();
});

//# sourceMappingURL=index.controller.js.map
