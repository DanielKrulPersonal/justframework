import {Controller} from "justFramework/controller";


class indexController extends Controller {
	public initialize() {
		this.templateLoadFadeIn = true;
	}
	public runEvent() {
		super.loadMainTemplate({});
	}
	public setRoutes(subPage: string): void {

	}
}
indexController.register();