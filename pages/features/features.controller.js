var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "justFramework/controller"], factory);
    }
})(function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var controller_1 = require("justFramework/controller");
    var featuresController = /** @class */ (function (_super) {
        __extends(featuresController, _super);
        function featuresController() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        featuresController.prototype.initialize = function () {
            this.templateLoadFadeIn = true;
        };
        featuresController.prototype.runEvent = function () {
            var data = _super.prototype.getModelData.call(this);
            var obvod = data.sirka + data.delka;
            _super.prototype.loadMainTemplate.call(this, {
                'hlaska': 'Jaffa kree <strong>KREE</strong>',
                'unixTime': '1550445187'
            });
        };
        featuresController.prototype.setRoutes = function (subPage) {
        };
        return featuresController;
    }(controller_1.Controller));
    featuresController.register();
});

//# sourceMappingURL=features.controller.js.map
