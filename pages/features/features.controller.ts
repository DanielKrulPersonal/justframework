import {Controller} from "justFramework/controller";

class featuresController extends Controller {
	public initialize() {
		this.templateLoadFadeIn = true;
	}
	public runEvent() {
		let data = super.getModelData();
		let obvod = data.sirka + data.delka;

		super.loadMainTemplate({
			'hlaska': 'Jaffa kree <strong>KREE</strong>',
			'unixTime': '1550445187'
		});
	}
	public setRoutes(subPage: string): void {

	}
}
featuresController.register();